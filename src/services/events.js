import {
  addDoc,
  createColListener,
  createColListenerWhere,
  deleteDoc,
  getColData,
  getColDataWhere,
  getColRef,
  getDocRef,
} from "./firestore";
import { getSingleState } from "./state";
import { getUserFromStore } from "../helpers/store";
import { deleteImageFromUrlService } from "./storage";
import { firestore } from "./firebase";
import { userCan } from "../rbac/Can";

const EVENTS_ENDPOINT = "events/";

export const createEventService = async (values) => {
  const user = getUserFromStore();

  const newEvent = {
    stateDetails: await getSingleState(values.state),
    spotlight: values.spotlight || false,
    address: values.address,
    mapCoordinates: values.mapCoordinates.center,
    title: values.title,
    image: values.image,
    description: values.description,
    state: values.state,
    author: user.toJson(),
    authorUid: user.id,
    place: values.place,
    dateTime: values.dateTime.toDate(),
    createdAt: new Date(),
  };

  const newsRef = getColRef(EVENTS_ENDPOINT, {});
  return await newsRef.add(newEvent);
};

export const updateEventService = async (eventId, data) => {
  const user = getUserFromStore();

  console.log("on update event");
  console.log(data);
  console.log(user);
  const eventRef = getDocRef(`${EVENTS_ENDPOINT}${eventId}`);
  return await eventRef.update(data);
};

export const editEventService = async (eventId, values) => {
  const user = getUserFromStore();

  const event = {
    stateDetails: await getSingleState(values.state),
    spotlight: values.spotlight || false,
    address: values.address,
    place: values.place,
    title: values.title,
    image: values.image,
    description: values.description,
    state: values.state,
    author: user.toJson(),
    authorUid: user.id,
    dateTime: values.dateTime.toDate(),
    updatedAt: new Date(),
    happened: values.happened || false,
  };

  if (values.mapCoordinates.center)
    event.mapCoordinates = values.mapCoordinates.center;

  const newsRef = getDocRef(`${EVENTS_ENDPOINT}${eventId}`);
  return await newsRef.update(event);
};

export const createEventsByStateListenerService = (
  { status, stateId },
  callback
) => {
  const user = getUserFromStore();
  if (!userCan({ perform: "events:see_all" })) {
    stateId = user._state._id;
  }

  let ref = firestore.collection("events");

  if (status !== null) ref = ref.where("happened", "==", status);
  if (stateId !== null) ref = ref.where("state", "==", stateId);

  ref.orderBy("dateTime", "asc");

  ref.onSnapshot((snapshot) => {
    const data = [];
    snapshot.docs.forEach((item) => data.push({ id: item.id, ...item.data() }));
    callback(data);
  });
};

export const createEventsListenerService = (callback) => {
  createColListener("events", callback, { orderBy: "dateTime" });
};

export const getEventsByStateService = async (stateId) => {
  return await getColDataWhere("events", {
    orderBy: "dateTime",
    orderDirection: "asc",
    where: { field: "state", operator: "==", value: stateId },
  });
};

export const getEventsService = async () => {
  return await getColData("events", {
    orderBy: "dateTime",
    orderDirection: "asc",
  });
};

export const deleteEventService = async (eventId) => {
  const newsRef = getDocRef(`events/${eventId}`);
  return await newsRef.delete();
};

export const createEventImageGalleryListenerService = (eventId, callback) => {
  createColListener(`events/${eventId}/gallery`, callback, {});
};

export const deleteEventImageGalleryService = async (eventId, image) => {
  await deleteDoc(`events/${eventId}/gallery/${image.id}`);
  await deleteImageFromUrlService(image.url);
};

export const createEventGalleryImageService = async (eventId, data) => {
  return await addDoc(`events/${eventId}/gallery`, data);
};
