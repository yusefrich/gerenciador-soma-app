import React, { useState } from "react";
import DropdownMenu from "../../components/DropdownMenu";
import { deleteEventService, editEventService } from "../../services/events";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { Icon, Modal } from "antd";
import EditEventForm from "./EditEventForm";

const { confirm } = Modal;

const EventOptions = ({ event }) => {
  const [loading, setLoading] = useState(false);
  const [loadingHappenedEvent, setLoadingHappenedEvent] = useState(false);
  const [showEditEventDrawer, setShowEditEventDrawer] = useState(false);

  const deleteEvent = async () => {
    try {
      setLoading(true);
      await deleteEventService(event.id);
      notifySuccess({ title: "Tudo certo", message: "O evento foi excluído" });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  const setHappenedEvent = async () => {
    try {
      setLoadingHappenedEvent(true);
      await editEventService(event.id, { ...event, happened: !event.happened });
      notifySuccess({
        title: "Tudo certo",
        message: `Evento marcado como ${
          !event.happened ? "'Já Realizado'" : "'Não realizado'"
        }`,
      });
      setLoadingHappenedEvent(false);
    } catch (e) {
      setLoadingHappenedEvent(false);
      notifyError(e);
    }
  };

  const promptToDeleteEvent = async () => {
    confirm({
      okText: "Quero excluir",
      title: "Você tem certeza de que deseja excluir este evento?",
      content: "Esta operação não pode ser desfeita",
      onOk() {
        deleteEvent();
      },
    });
  };

  const options = [
    {
      label: (
        <>
          <Icon type="edit" />
          Editar
        </>
      ),
      onSelect: () => setShowEditEventDrawer(true),
    },
    {
      label: loadingHappenedEvent ? (
        <Icon type="loading" />
      ) : (
        <>
          <Icon type="check" />
          {event.happened
            ? "Marcar como não realizado"
            : "Marcar como realizado"}
        </>
      ),
      onSelect: setHappenedEvent,
    },
    {
      label: loading ? (
        <Icon type="loading" />
      ) : (
        <>
          <Icon type="delete" />
          Excluir
        </>
      ),
      onSelect: promptToDeleteEvent,
    },
  ];

  return (
    <>
      <DropdownMenu label="Opções" options={options} />
      <EditEventForm
        isOpen={showEditEventDrawer}
        onClose={() => setShowEditEventDrawer(false)}
        event={event}
      />
    </>
  );
};

export default EventOptions;
