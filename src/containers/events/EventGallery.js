import React, { useEffect, useState } from "react";
import { Button, Divider, Icon, Modal, Tag, Upload } from "antd";

import { uploadFileService } from "../../services/storage";
import {
  createEventGalleryImageService,
  createEventImageGalleryListenerService,
} from "../../services/events";
import Spin from "../../components/Spin";
import SingleImageGallery from "./SingleImageGallery";

export default ({ event }) => {
  const [modalOpen, isModalOpen] = useState(false);
  const [loading, isLoading] = useState(true);
  const [imageList, setImageList] = useState(true);

  useEffect(() => {
    if (modalOpen) {
      getImagesList();
    }
  }, [modalOpen]);

  const getImagesList = async () => {
    try {
      isLoading(true);
      createEventImageGalleryListenerService(event.id, (imageList) => {
        setImageList(
          imageList.map((image) => ({
            id: image.id,
            url: image.url,
            src: image.url,
            thumbnail: image.thumbnail || image.url,
            thumbnailWidth: 100,
            thumbnailHeight: 100,
          }))
        );
        isLoading(false);
      });
    } catch (e) {
      console.log(e);
    }
  };

  const createEventGalleryImage = async (data) => {
    createEventGalleryImageService(event.id, data);
  };

  const onUpload = ({ onSuccess, onError, file, onProgress }) => {
    const onSuccessCallback = (url, fileName) => {
      createEventGalleryImage({ url, fileName });
      onSuccess("done");
    };

    uploadFileService({
      url: `events/${event.id}/gallery`,
      onSuccessCallback,
      onError,
      file,
      onProgress,
    });
  };

  const beforeUpload = (file) => {
    const supportedFileType = ["image/png", "image/jpg", "image/jpeg"];
    if (supportedFileType.includes(file.type)) {
      return true;
    }
    return false;
  };

  return (
    <>
      <Tag
        style={{ marginTop: 5 }}
        color="purple"
        type="primary"
        onClick={() => isModalOpen(!modalOpen)}
      >
        <Icon type="picture" /> Galeria
      </Tag>
      {modalOpen && (
        <Modal
          title={event.title}
          width="80%"
          onCancel={() => isModalOpen(false)}
          visible={modalOpen}
          footer={[
            <Button
              onClick={() => isModalOpen(false)}
              style={{ marginRight: 8 }}
            >
              Fechar
            </Button>,
          ]}
        >
          {loading ? (
            <Spin />
          ) : (
            <>
              <div style={{ display: "flex" }}>
                {imageList.map((image) => (
                  <SingleImageGallery
                    key={image.id}
                    event={event}
                    image={image}
                  />
                ))}
              </div>
              <Divider />
              <div className="clearfix">
                <Upload
                  multiple
                  accept="jpg, jpeg, png"
                  name="image"
                  beforeUpload={beforeUpload}
                  customRequest={onUpload}
                  listType="picture-card"
                >
                  {uploadButton}
                </Upload>
              </div>
            </>
          )}
        </Modal>
      )}
    </>
  );
};

const uploadButton = (
  <div>
    <Icon type="plus" />
    <div className="ant-upload-text">Upload</div>
  </div>
);
