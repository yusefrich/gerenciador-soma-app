import React, { useEffect, useState } from "react";
import {
  Button,
  Checkbox,
  Col,
  DatePicker,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Upload,
} from "antd";
import locale from "antd/es/date-picker/locale/pt_BR";
import GoogleMapReact from "google-map-react";

import {
  deleteImageFromUrlService,
  uploadFileService,
} from "../../services/storage";
import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { useSelector } from "react-redux";
import { userCan } from "../../rbac/Can";
import { beforeUpload } from "../../helpers/forms";
import { getAddressService } from "../../services/commonn";
import mapPin from "../../assets/imgs/pin.svg";
import { editEventService } from "../../services/events";
import { firestoreTimeToMoment } from "../../helpers/common";

const { Option } = Select;
const { Search } = Input;

const MapPin = ({ text }) => (
  <div>
    <img src={mapPin} width={20} alt="" />
  </div>
);

export default Form.create({ name: "edit_event_form" })(
  ({
    event,
    isOpen,
    onClose,
    form: { getFieldDecorator, validateFieldsAndScroll, resetFields },
  }) => {
    const [loading, isLoading] = useState(false);
    const [loadingAddress, isLoadingAddress] = useState(false);
    const [mapsCenter, setMapsCenter] = useState(event.mapCoordinates);
    const [buttonDisabled, isButtonDisabled] = useState(false);
    const [uploadingImage, isUploadingImage] = useState(false);
    const [windowWidth, setWindowWidth] = useState("90%");

    const [address, setAddress] = useState("");
    const [draggableMap, setDraggableMap] = useState(true);

    const [imageUrl, setImageUrl] = useState(event.image);
    const { states } = useSelector((state) => state.states);
    let timer = null;

    const user = getUserFromStore();

    useEffect(() => {
      return () => (timer = null);
    }, [timer]);

    const handleSearchAddress = async (query) => {
      isLoadingAddress(true);
      const address = await getAddressService(query);
      setMapsCenter(address.geometry.location);
      isLoadingAddress(false);
    };

    const handleSearchAddressInput = (e) => {
      clearTimeout(timer);
      setAddress(e.target.value);
      timer = setTimeout(() => {
        handleSearchAddress(address);
      }, 1000);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            if (imageUrl) values.image = imageUrl;

            await editEventService(event.id, values);
            notifySuccess({
              title: "Ok!",
              message: "O evento foi atualizado",
            });
            resetFields();
            setImageUrl("");
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const onUpload = ({ onSuccess, onError, file, onProgress }) => {
      const onSuccessCallback = (url) => {
        setImageUrl(url);
        onSuccess("done");
      };
      uploadFileService({
        url: `events/${user.id}`,
        onSuccessCallback,
        onError,
        file,
        onProgress,
      });
    };

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        isUploadingImage(true);
        isButtonDisabled(true);
        setImageUrl("");
        return;
      }
      if (info.file.status === "done") {
        isButtonDisabled(false);
        isUploadingImage(false);
      }
    };

    const IncreaseWindowSizeButton = () => {
      if (windowWidth === 520)
        return (
          <Icon
            type="fullscreen"
            onClick={() => setWindowWidth("90%")}
            style={{ marginRight: 20 }}
          />
        );
      else
        return (
          <Icon
            type="fullscreen-exit"
            onClick={() => setWindowWidth(520)}
            style={{ marginRight: 20 }}
          />
        );
    };

    const onCancelForm = async () => {
      onClose();
    };

    const onMoveMap = (childKey, childProps, mouse) => {
      setMapsCenter({ lat: mouse.lat, lng: mouse.lng });
      setDraggableMap(false);
    };
    return (
      <>
        <Drawer
          title={[<IncreaseWindowSizeButton />, "Criar Evento"]}
          width={windowWidth}
          onClose={onCancelForm}
          visible={isOpen}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Titulo">
                  {getFieldDecorator("title", {
                    initialValue: event.title,
                    rules: [{ required: true, message: "Insira um título" }],
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Dia e Hora">
                  {getFieldDecorator("dateTime", {
                    initialValue: firestoreTimeToMoment(event.dateTime),
                    rules: [
                      { required: true, message: "Selecione dia e horário" },
                    ],
                  })(
                    <DatePicker
                      locale={locale}
                      showTime
                      format="DD/MM/YYYY - HH:mm"
                      placeholder="Selecione dia e horário"
                    />
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator("spotlight", {
                    valuePropName: "checked",
                    initialValue: event.spotlight,
                  })(<Checkbox>Destacar Evento</Checkbox>)}
                </Form.Item>
                <Form.Item label="Descrição">
                  {getFieldDecorator("description", {
                    initialValue: event.description,
                    rules: [
                      { required: true, message: "Insira uma Descrição" },
                    ],
                  })(<Input.TextArea rows={5} />)}
                </Form.Item>
                <Form.Item label="Estado">
                  {getFieldDecorator("state", {
                    initialValue: event.state,
                    rules: [
                      { required: true, message: "Selecione uma estado" },
                    ],
                  })(
                    <Select disabled={!userCan({ perform: "news:edit_state" })}>
                      {states.map((state) => (
                        <Option key={state.id} value={state.id}>
                          {state.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
                <Form.Item label="Imagem de Destaque">
                  {getFieldDecorator("image", {
                    initialValue: event.image,
                    rules: [{ required: true, message: "Insira uma imagem" }],
                  })(
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      beforeUpload={beforeUpload}
                      customRequest={onUpload}
                      onChange={handleChangeImage}
                    >
                      {imageUrl ? (
                        <img
                          src={imageUrl}
                          alt="avatar"
                          style={{ width: "100%" }}
                        />
                      ) : (
                        <div>
                          <Icon type={uploadingImage ? "loading" : "plus"} />
                          <div className="ant-upload-text">Upload</div>
                        </div>
                      )}
                    </Upload>
                  )}
                  <Divider />
                </Form.Item>

                <Form.Item label="Local">
                  {getFieldDecorator("place", {
                    initialValue: event.place,
                    rules: [{ required: true, message: "Informe o local" }],
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Endereço">
                  {getFieldDecorator("address", {
                    initialValue: event.address,
                    rules: [{ required: true, message: "Informe o endereço" }],
                  })(
                    <Search
                      onChange={handleSearchAddressInput}
                      onPressEnter={handleSearchAddress}
                      placeholder="Pesquise o endereço"
                      enterButton
                      onSearch={handleSearchAddress}
                      loading={loadingAddress}
                    />
                  )}
                </Form.Item>

                <Form.Item label="Local">
                  <div style={{ width: "100%", height: 300 }}>
                    {getFieldDecorator("mapCoordinates", {
                      initialValue: event.mapCoordinates,
                      rules: [{ required: true, message: "Selecione o Local" }],
                    })(
                      <GoogleMapReact
                        draggable={draggableMap}
                        bootstrapURLKeys={{
                          key: "AIzaSyChxNdxSPluskBuBgvgkgtrvtdnRu6VQTM",
                        }}
                        center={{
                          lat: mapsCenter.lat,
                          lng: mapsCenter.lng,
                        }}
                        defaultCenter={{
                          lat: mapsCenter.lat,
                          lng: mapsCenter.lng,
                        }}
                        onChildMouseUp={() => setDraggableMap(true)}
                        onChildMouseDown={onMoveMap}
                        onChildMouseMove={onMoveMap}
                        defaultZoom={15}
                      >
                        <MapPin lat={mapsCenter.lat} lng={mapsCenter.lng} />
                      </GoogleMapReact>
                    )}
                  </div>
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Button onClick={onCancelForm} style={{ marginRight: 8 }}>
                Cancelar
              </Button>
              <Button
                disabled={buttonDisabled}
                loading={loading}
                htmlType="submit"
                type="primary"
              >
                Salvar
              </Button>
            </div>
          </Form>
        </Drawer>
      </>
    );
  }
);
