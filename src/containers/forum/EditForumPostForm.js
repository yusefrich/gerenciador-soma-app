import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Switch,
  Tag,
} from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { updateForumPostService } from "../../services/forum/forumPosts";
import { getForumCategoriesService } from "../../services/forum/forumCategories";
import StateFilter from "../forms/StateFilter";
import rolesEnum from "../../models/rolesEnum";
import { getUserFromStore } from "../../helpers/store";

export default Form.create({ name: "edit_forum_post_form" })(
  ({
    post,
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
      resetFields,
      setFieldsValue,
    },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [loading, isLoading] = useState(false);
    const [categories, setCategories] = useState([]);
    const user = getUserFromStore();

    useEffect(() => {
      getForumCategories();
    }, []);

    const getForumCategories = async () => {
      try {
        const categories = await getForumCategoriesService();
        setCategories(categories);
      } catch (e) {}
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await updateForumPostService(post.id, values);
            notifySuccess({
              title: "Ok!",
              message: "O post foi atualizado",
            });
            resetFields();
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    return (
      <>
        <Tag
          color="blue"
          type="primary"
          onClick={() => isDrawerOpen(!drawerOpen)}
        >
          <Icon type="edit" /> editar
        </Tag>
        <Drawer
          title="Criar Novo Post"
          onClose={() => isDrawerOpen(false)}
          width={520}
          visible={drawerOpen}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Título">
                  {getFieldDecorator("title", {
                    initialValue: post.title,
                    rules: [
                      {
                        required: true,
                        message: "Insira um título",
                      },
                    ],
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Texto (opcional)">
                  {getFieldDecorator("text", { initialValue: post.text })(
                    <Input />
                  )}
                </Form.Item>
                <Divider />
                <Form.Item label="Estado">
                  {getFieldDecorator("stateId", {
                    initialValue: post.state,
                    rules: [{ required: true, message: "Selecione um estado" }],
                  })(
                    <StateFilter
                      disabled={user._role !== rolesEnum.SUPER_ADMIN}
                      initialValue={post.state}
                      onSelect={(stateId) => setFieldsValue({ stateId })}
                    />
                  )}
                </Form.Item>
                <Form.Item label="Categoria">
                  {getFieldDecorator("category", {
                    initialValue: post.category && post.category.id,
                    rules: [
                      { required: true, message: "Selecione uma categoria" },
                    ],
                  })(
                    <Select>
                      {categories.map((category) => (
                        <Select.Option key={category.id} value={category.id}>
                          {category.name}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
                <Form.Item label="Postagem ativa">
                  {getFieldDecorator("isActive", {
                    initialValue: post.isActive,
                  })(<Switch defaultChecked={post.isActive} />)}
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Button
                onClick={() => isDrawerOpen(false)}
                style={{ marginRight: 8 }}
              >
                Cancelar
              </Button>
              <Button loading={loading} htmlType="submit" type="primary">
                Salvar
              </Button>
            </div>
          </Form>
        </Drawer>
      </>
    );
  }
);
