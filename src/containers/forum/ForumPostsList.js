import React, { useEffect, useState } from "react";
import {
  Col,
  Descriptions,
  Divider,
  Icon,
  Popconfirm,
  Row,
  Select,
  Table,
  Tag,
} from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import {
  createPostsListener,
  deleteForumPostService,
} from "../../services/forum/forumPosts";
import ForumCategoriesSelect from "./ForumCategoriesSelect";
import EditForumPostForm from "./EditForumPostForm";
import StateFilter from "../forms/StateFilter";

const { Option } = Select;

export default () => {
  const [posts, setPosts] = useState([]);
  const [loading, isLoading] = useState(false);
  const [isActive, setActiveFilter] = useState("all");
  const [activeState, setActiveState] = useState(null);
  const [category, setCategory] = useState("all");

  useEffect(() => {
    getPosts();
  }, [isActive, category, activeState]);

  const getPosts = async () => {
    try {
      isLoading(true);
      createPostsListener(
        { filters: { isActive, categoryId: category, stateId: activeState } },
        (categories) => {
          setPosts(categories);
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Título",
      dataIndex: "title",
      key: "title",
      width: "30%",
    },
    {
      title: "Texto",
      dataIndex: "text",
      key: "text",
      width: "50%",
    },
    {
      title: "Categoria",
      dataIndex: "category.name",
      key: "category.id",
      width: "10%",
    },
    {
      title: "Ativo",
      key: "id",
      width: "5%",
      render: (post) => {
        const iconProperties = post.isActive
          ? { type: "check-circle", style: { color: "green" }, theme: "filled" }
          : {
              type: "close-circle",
              style: { color: "red" },
            };
        return <Icon {...iconProperties} />;
      },
    },
    {
      title: "",
      dataIndex: "",
      key: "",
      render: (post) => (
        <>
          <EditForumPostForm post={post} />
          <DeletePostButton post={post} />
        </>
      ),
    },
  ];

  return (
    <div>
      <Row gutter={24}>
        <Col span={4}>
          <StateFilter onSelect={(stateId) => setActiveState(stateId)} />
        </Col>
        <Col span={4}>
          <ForumCategoriesSelect
            onChange={(value) => setCategory(value)}
            value={category}
          />
        </Col>
        <Col span={3}>
          <Select
            defaultValue={isActive}
            onChange={(value) => setActiveFilter(value)}
          >
            <Option value="all">Todos os posts</Option>
            <Option value={true}>Posts Ativos</Option>
            <Option value={false}>Posts Inativos</Option>
          </Select>
        </Col>
      </Row>
      <Row gutter={24}>
        <Divider />
        <Col>
          <Table
            columns={columns}
            dataSource={posts}
            loading={loading}
            rowExpandable
            expandedRowRender={(post) => <PostDetails post={post} />}
          />
        </Col>
      </Row>
    </div>
  );
};

const PostDetails = ({ post }) => {
  return (
    <Row gutter={16}>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Fórum Post
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Título">{post.title}</Descriptions.Item>
        </Descriptions>
        {post.text && (
          <Descriptions layout="vertical" bordered size="small">
            <Descriptions.Item label="Texto">{post.text}</Descriptions.Item>
          </Descriptions>
        )}
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Categoria">
            <Tag color="blue">{post.category?.name}</Tag>
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Estado">
            {post.stateDetails.name}
          </Descriptions.Item>
        </Descriptions>
      </Col>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Autoria
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Autor">
            {post.author.name}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Empresa">
            {post.author.business}
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};

const DeletePostButton = ({ post }) => {
  const [loading, setLoading] = useState(false);

  const deleteForumPost = async () => {
    try {
      setLoading(true);
      await deleteForumPostService(post.id);
      notifySuccess({ title: "Tudo certo", message: "Post removido" });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  return (
    <Popconfirm
      title={
        <p>
          Você tem quer deletar mesmo? <br /> Todas as respostas já enviadas
          para este post
          <br /> também serão excluídas
        </p>
      }
      onConfirm={deleteForumPost}
      okText="Sim"
      cancelText="Não"
    >
      <Tag color="red"> {loading && <Icon type="loading" />} excluir</Tag>
    </Popconfirm>
  );
};
