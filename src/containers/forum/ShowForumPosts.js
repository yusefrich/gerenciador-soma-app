import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Switch,
  Tag,
} from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { updateForumPostService, updateLikesService, createPostRepliesListenerService, replyMainPostService } from "../../services/forum/forumPosts";
import { getForumCategoriesService } from "../../services/forum/forumCategories";
import StateFilter from "../forms/StateFilter";
import rolesEnum from "../../models/rolesEnum";
import { getUserFromStore } from "../../helpers/store";
import styled from "styled-components";
import { firestoreTimeToMoment, formattedDate } from "../../helpers/common";
import like from "../../assets/imgs/thumb.png"
import reply from "../../assets/imgs/reply.png"

const ForumContainer = styled.div`
margin-bottom: 15px;
margin-left: 15px;
margin-right: 15px;
cursor: pointer;
`;
const ForumUserInfo = styled.div`
display: flex;
justify-content: start;
`;
const ForumPostInfo = styled.div`
display: flex;
justify-content: space-between;
`;
const PostData = styled.div`
text-align: right;
`;
const DiscountTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: 600;
font-size: 20px;
line-height: 110%;
color: #9096B4;
`;
const ForumUserName = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 16px;
line-height: 175.9%;
color: #9096B4;
margin-left: 15px;
`;


const DrawerContentDetail = styled.div`
text-align: center;
margin: 15px;
`;
const DetailTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: bold;
font-size: 22px;
line-height: 120%;
margin-bottom: 15px;
`;
const DetailParagraph = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 16px;
line-height: 120%;

color: #9096B4;
`;

const CategoryTag = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 500;
font-size: 14px;
line-height: 175.9%;
color: #fff;
border: 1px solid #fff;
border-radius: 90px;
display: inline;
padding: 2px 15px;
`;
const DetailShoTitle = styled.h4`
font-family: Poppins;
font-style: normal;
font-weight: 500;
font-size: 18px;
line-height: 240%;
margin-bottom: 16px;

`;
const DetailShoLocal = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 14px;
line-height: 120%;
`;
const ShareContentArea = styled.div`
display: block;
margin-top: 10px;
`;
const LikeArea = styled.button`
background: transparent;
border: none;
cursor: pointer;
padding: 10px;
span{

  font-family: Poppins;
  font-weight: 300;
  font-style: normal;
  font-size: 18px;
  line-height: 194.4%;
  color: #9096B4;
  padding-left: 15px;
  top: 5px;
  position: relative;
}
`;

export default Form.create({ name: "edit_forum_post_form" })(
  ({
    post,
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
      resetFields,
      setFieldsValue,
    },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [replyDrawerOpen, isReplyDrawerOpen] = useState(false);
    const [loading, isLoading] = useState(false);
    const [categories, setCategories] = useState([]);
    const [postLikes, setPostLikes] = useState([]);
    const [replies, setReplies] = useState([]);
    const user = getUserFromStore();
    const [contentLimite, setContentLimit] = useState([]);

    useEffect(() => {
      getForumCategories();
      createPostRepliesListener();
      setPostLikes(post.likes);
      setContentLimit(0);

    }, []);

    const getForumCategories = async () => {
      try {
        const categories = await getForumCategoriesService();
        setCategories(categories);
      } catch (e) { }
    };
    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await replyMainPostService(post.id, values);
            notifySuccess({
              title: "Ok!",
              message: "Sua resposta foi registrada",
            });
            resetFields();
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };
  
    /* const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await updateForumPostService(post.id, values);
            notifySuccess({
              title: "Ok!",
              message: "O post foi atualizado",
            });
            resetFields();
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    }; */

    const likeNews = async () => {
      const likes = post.likes && post.likes.length ? post.likes : [];
  
      const postLikes = likes.includes(user.id)
        ? likes.filter(like => like !== user.id)
        : likes.concat([user.id]);
      
        setPostLikes(postLikes);
  
      await updateLikesService(post.id, postLikes);
    };
    const createPostRepliesListener = () => {
      isLoading(true);
      createPostRepliesListenerService(post.id, replies => {
        isLoading(false);
        setReplies(replies);
      });
    };

    function maxLengthCheckContent(object){
      if (object.currentTarget.value.length > object.currentTarget.maxLength) {
        object.currentTarget.value = object.currentTarget.value.slice(0, object.currentTarget.maxLength)
      }

      setContentLimit(object.currentTarget.value.length)

    }

    const ForumPostReplies = () => (
      <>
        {replies.map((row) => {
          return (<>
          <ForumUserInfo>
            <img style={{ borderRadius: 90, marginTop: 20 }} src={row.author.photoURL} width="40px" height="40px" alt="" />
            <div>
              <ForumUserName style={{ marginTop: 15, marginBottom: 0 }}>{row.author.name}</ForumUserName>
              <ForumUserName style={{ marginTop: 0 }}>
                {formattedDate(
                  firestoreTimeToMoment(row.createdAt),
                  "DD/MM/YYYY - HH:mm"
                )}
              </ForumUserName>
            </div>
          </ForumUserInfo>

            <DetailParagraph>{row.text}</DetailParagraph>
          </>)
        }
        )}
      </>
    );
  
  
    return (
      <>
        <ForumContainer onClick={() => isDrawerOpen(!drawerOpen)} >
          <DiscountTitle>{post.title}</DiscountTitle>
          <ForumPostInfo>
            <ForumUserInfo>
              <img style={{ borderRadius: 90, marginTop: 20 }} src={post.author.photoURL} width="40px" height="40px" alt="" />
              <div>
                <ForumUserName style={{ marginTop: 15, marginBottom: 0 }}>{post.author.name}</ForumUserName>
                <ForumUserName style={{ marginTop: 0 }}>{post.author.business}</ForumUserName>
              </div>
            </ForumUserInfo>
            <PostData>
              <CategoryTag>{post.category.name}</CategoryTag>
              <ForumUserName style={{ marginTop: 15 }}>
                {formattedDate(
                  firestoreTimeToMoment(post.createdAt),
                  "DD/MM/YYYY - HH:mm"
                )}
              </ForumUserName>
            </PostData>
          </ForumPostInfo>
        </ForumContainer>
        <Divider style={{ background: "#090C1D" }} />
        <Drawer
          title="Post"
          onClose={() => isDrawerOpen(false)}
          width={520}
          visible={drawerOpen}
          drawerStyle={{ background: "#151A2E" }}
          bodyStyle={{ paddingBottom: 80, background: "#151A2E" }}
          headerStyle={{ background: "#9096B4", borderBottom: "1px solid #090C1D", color: "#9096B4" }}
        >
          <ForumUserInfo>
            <img style={{ borderRadius: 90, marginTop: 20 }} src={post.author.photoURL} width="40px" height="40px" alt="" />
            <div>
              <ForumUserName style={{ marginTop: 15, marginBottom: 0 }}>{post.author.name}</ForumUserName>
              <ForumUserName style={{ marginTop: 0 }}>{post.author.business}</ForumUserName>
            </div>
          </ForumUserInfo>
          <DiscountTitle>{post.title}</DiscountTitle>
          <DetailParagraph>{post.text}</DetailParagraph>
          <ForumUserName style={{ marginTop: 15, textAlign: 'right' }}>
            {formattedDate(
              firestoreTimeToMoment(post.createdAt),
              "DD/MM/YYYY - HH:mm"
            )}
          </ForumUserName>
          <ShareContentArea> 
            <LikeArea onClick={likeNews}>{/*  */}
              <img  src={like} alt="like button"/>
              <span>{postLikes?.length || 0}</span>
            </LikeArea>
            <LikeArea onClick={() => isReplyDrawerOpen(!replyDrawerOpen)}>
              <img  src={reply} alt="like button"/>
              <span>{post.repliesCounter || 0}</span>
            </LikeArea>
          </ShareContentArea>
          {replyDrawerOpen && 
            <div className="user_post_form_sm">
              <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
                <Row gutter={24}>
                  <Col span={24}>
                  <Form.Item label="Texto">
                    {getFieldDecorator("text", {
                      rules: [
                        {
                          required: true,
                          message: "Insira a mensagem",
                        },
                      ],
                    })(
                      <Input maxLength={250} onInput={maxLengthCheckContent} />
                    )}
                  </Form.Item>
                  <p  style={{position: "relative", top: -16, color: "#9096B4"}} >{contentLimite}/250</p>
                  <Button loading={loading} htmlType="submit" type="primary">
                    Salvar
                  </Button>

                  </Col>
                </Row>

              </Form>
            </div>
          }
          <Divider style={{background: "#9096b4"}}/>
          {replies && <ForumPostReplies />}
        </Drawer>
      </>
    );
  }
);
