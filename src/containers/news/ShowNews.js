import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Card,
  Select,
  Tag,
  Upload,
} from "antd";
import { getAreasOfInterestsService } from "../../services/interests";
import { uploadFileService } from "../../services/storage";
import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { updateNewsService, updateLikesService } from "../../services/news";
import { useSelector } from "react-redux";
import { userCan } from "../../rbac/Can";
import { beforeUpload } from "../../helpers/forms";
import Editor from "../../components/Editor";
import htmlToDraft from "html-to-draftjs";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import draftToHtml from "draftjs-to-html";
import { firestoreTimeToMoment, formattedDate } from "../../helpers/common";
import styled from "styled-components";
import like from "../../assets/imgs/thumb.png"
import share from "../../assets/imgs/share.png"
const { Option } = Select;

const CardTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: 600;
font-size: 22px;
line-height: 110%;
color: #9096B4;
`;
const NewsTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: 600;
font-size: 22px;
margin-bottom: 30px;
line-height: 110%;
color: #9096B4;
`;
const NewsContent = styled.div`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 18px;
line-height: 120%;
color: #9096B4 !important;
h3{
  color: #9096B4 !important;
  span{
    color: #9096B4 !important;
  }
}
`;
const NewsContainer = styled.div`
padding-left: 204px;
padding-right: 204px;
@media  (max-width: 800px) {
  padding-left: 24px;
  padding-right: 24px;
}
`;
const ShareContentArea = styled.div`
display: flex;
justify-content: space-between;
margin-top: 10px;
`;
const LikeArea = styled.button`
background: transparent;
border: none;
cursor: pointer;
padding: 10px;
span{

  font-family: Poppins;
  font-weight: 300;
  font-style: normal;
  font-size: 18px;
  line-height: 194.4%;
  color: #9096B4;
  padding-left: 15px;
  top: 5px;
  position: relative;
}
`;
const ShareArea = styled.div`
background: transparent;
border: none;
cursor: pointer;
padding: 10px;

font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 18px;
line-height: 194.4%;
color: #9096B4;
`;

export default Form.create({ name: "add_news_form" })(
  ({
    news,
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
      validateFields,
      getFieldValue,
      resetFields,
    },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [areas, setAreas] = useState([]);
    const [postLikes, setPostLikes] = useState([]);
    const [loading, isLoading] = useState(false);
    const [buttonDisabled, isButtonDisabled] = useState(false);
    const [uploadingImage, isUploadingImage] = useState(false);
    const [windowWidth, setWindowWidth] = useState("90%");
    const [imageUrl, setImageUrl] = useState(news.image);
    const { states } = useSelector((state) => state.states);

    const user = getUserFromStore();

    useEffect(() => {
      /* console.log(news); */
      setPostLikes(news.likes);
      getAreas();
    }, []);

    const getAreas = async () => {
      const areas = await getAreasOfInterestsService();
      setAreas(areas);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            if (imageUrl) values.image = imageUrl;

            values.author = user.toJson();
            values.authorUid = user.id;
            values.updatedAt = new Date();
            values.text = draftToHtml(
              convertToRaw(values.text.getCurrentContent())
            );

            await updateNewsService(news.id, values);
            notifySuccess({
              title: "Ok!",
              message: "A notícia foi atualizada",
            });
            window.location.reload();
            resetFields();
            setImageUrl("");
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const onUpload = ({ onSuccess, onError, file, onProgress }) => {
      const onSuccessCallback = (url) => {
        setImageUrl(url);
        onSuccess("done");
      };
      uploadFileService({
        url: `news/${user.id}`,
        onSuccessCallback,
        onError,
        file,
        onProgress,
      });
    };

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        isUploadingImage(true);
        isButtonDisabled(true);
        setImageUrl("");
        return;
      }
      if (info.file.status === "done") {
        isButtonDisabled(false);
        isUploadingImage(false);
      }
    };

    const IncreaseWindowSizeButton = () => {
      if (windowWidth === 520)
        return (
          <Icon
            type="fullscreen"
            onClick={() => setWindowWidth("90%")}
            style={{ marginRight: 20 }}
          />
        );
      else
        return (
          <Icon
            type="fullscreen-exit"
            onClick={() => setWindowWidth(520)}
            style={{ marginRight: 20 }}
          />
        );
    };
    const likeNews = async () => {
      const likes = news.likes && news.likes.length ? news.likes : [];
  
      const newLikes = likes.includes(user.id)
        ? likes.filter(like => like !== user.id)
        : likes.concat([user.id]);
      
        setPostLikes(newLikes);
  
      await updateLikesService(news.id, newLikes);
    };
  
  
    const onCancelForm = async () => {
      isDrawerOpen(false);
      setImageUrl("");
    };

    const contentBlock = htmlToDraft(news.text);
    const contentState = ContentState.createFromBlockArray(
      contentBlock.contentBlocks
    );
    const newsText = EditorState.createWithContent(contentState);

    return (
      <>
        <Card hoverable onClick={() => isDrawerOpen(!drawerOpen)} style={{ 
            width: '100%', 
            marginBottom: 2,
            background: '#090C1D',
            border: 0,
            borderLeft: '7px solid '+news.categoryDetails.color
          }}>
          <Row gutter={24}>
            {news.image && <>
              <Col span={18}>
              <CardTitle>{news.title}</CardTitle>
              <small style={{color: "#9096B4"}}>
                {formattedDate(
                  firestoreTimeToMoment(news.createdAt),
                  "DD MMM YYYY - hh:mm A"
                )}
                </small>

              </Col>
              <Col span={6}>
                <img  width="100%" height="auto" alt={news.slug} src={news.image} />
              </Col></>}
            {!news.image &&
              <Col >
                <CardTitle>{news.title}</CardTitle>
                <small style={{color: "#9096B4"}}>
                {formattedDate(
                  firestoreTimeToMoment(news.createdAt),
                  "DD MMM YYYY - hh:mm A"
                )}
                </small>

              </Col>}
          </Row>

        </Card>

        {/* <Tag
          color="blue"
          type="primary"
          onClick={() => isDrawerOpen(!drawerOpen)}
        >
          <Icon type="edit" /> Editar
        </Tag> */}
        <Drawer
          title={[<IncreaseWindowSizeButton />, "Notícia"]}
          width={windowWidth}
          onClose={onCancelForm}
          visible={drawerOpen}
          drawerStyle={{ background: "#151A2E" }}
          bodyStyle={{ paddingBottom: 80, background: "#151A2E" }}
          headerStyle={{ background: "#9096B4",borderBottom: "1px solid #090C1D", color: "#9096B4"  }}
        >
          <NewsContainer>

                <NewsTitle>{news.title}</NewsTitle>
                {news.image && 
                <img style={{maxWidth: '60%', marginLeft: 'auto', marginRight: 'auto', display: 'flex', marginBottom: 20}} src={news.image} width="100%" height="auto" alt={news.title}/>}
                <NewsContent dangerouslySetInnerHTML={{ __html: news.text }}></NewsContent>
                
                <ShareContentArea> 
                  <LikeArea onClick={likeNews}>
                    <img  src={like} alt="like button"/>
                    <span>{postLikes?.length}</span>
                   </LikeArea>
                  <ShareArea>
                    {/* <img  src={share} alt="share button"/> */}
                        <a className="resp-sharing-button__link" href={"https://www.facebook.com/sharer/sharer.php?u=https://noticias.sejasoma.com.br/noticia/"+news.slug+"%2F&amp;src=sdkpreparse"} target="_blank" rel="noopener" aria-label="">
                          <div className="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" className="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z"/></svg>
                            </div>
                          </div>
                        </a>

                        <a className="resp-sharing-button__link" href={"https://twitter.com/intent/tweet/?text=Noticias%20soma.&amp;url=https://noticias.sejasoma.com.br/noticia/"+news.slug} target="_blank" rel="noopener" aria-label="">
                          <div className="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" className="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z"/></svg>
                            </div>
                          </div>
                        </a>

                        <a className="resp-sharing-button__link" href={"mailto:?subject=Noticias%20soma.&amp;body=https://noticias.sejasoma.com.br/noticia/"+news.slug} target="_self" rel="noopener" aria-label="">
                          <div className="resp-sharing-button resp-sharing-button--email resp-sharing-button--small"><div aria-hidden="true" className="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M22 4H2C.9 4 0 4.9 0 6v12c0 1.1.9 2 2 2h20c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zM7.25 14.43l-3.5 2c-.08.05-.17.07-.25.07-.17 0-.34-.1-.43-.25-.14-.24-.06-.55.18-.68l3.5-2c.24-.14.55-.06.68.18.14.24.06.55-.18.68zm4.75.07c-.1 0-.2-.03-.27-.08l-8.5-5.5c-.23-.15-.3-.46-.15-.7.15-.22.46-.3.7-.14L12 13.4l8.23-5.32c.23-.15.54-.08.7.15.14.23.07.54-.16.7l-8.5 5.5c-.08.04-.17.07-.27.07zm8.93 1.75c-.1.16-.26.25-.43.25-.08 0-.17-.02-.25-.07l-3.5-2c-.24-.13-.32-.44-.18-.68s.44-.32.68-.18l3.5 2c.24.13.32.44.18.68z"/></svg>
                            </div>
                          </div>
                        </a>
                              

                        <a className="resp-sharing-button__link" 
                        href={"https://www.linkedin.com/shareArticle?mini=true&url=https://noticias.sejasoma.com.br/noticia/"+news.slug+"&title=Notícias%20Soma&summary="+news.title+"&source=LinkedIn"}target="_blank" rel="noopener" aria-label="">
                          <div className="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--small"><div aria-hidden="true" className="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.5 21.5h-5v-13h5v13zM4 6.5C2.5 6.5 1.5 5.3 1.5 4s1-2.4 2.5-2.4c1.6 0 2.5 1 2.6 2.5 0 1.4-1 2.5-2.6 2.5zm11.5 6c-1 0-2 1-2 2v7h-5v-13h5V10s1.6-1.5 4-1.5c3 0 5 2.2 5 6.3v6.7h-5v-7c0-1-1-2-2-2z"/></svg>
                            </div>
                          </div>
                        </a>

                        <a className="resp-sharing-button__link" href={"whatsapp://send?text=Notícias%20soma.%20https://noticias.sejasoma.com.br/noticia/"+news.slug} target="_blank" rel="noopener" aria-label="">
                          <div className="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--small"><div aria-hidden="true" className="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.1 3.9C17.9 1.7 15 .5 12 .5 5.8.5.7 5.6.7 11.9c0 2 .5 3.9 1.5 5.6L.6 23.4l6-1.6c1.6.9 3.5 1.3 5.4 1.3 6.3 0 11.4-5.1 11.4-11.4-.1-2.8-1.2-5.7-3.3-7.8zM12 21.4c-1.7 0-3.3-.5-4.8-1.3l-.4-.2-3.5 1 1-3.4L4 17c-1-1.5-1.4-3.2-1.4-5.1 0-5.2 4.2-9.4 9.4-9.4 2.5 0 4.9 1 6.7 2.8 1.8 1.8 2.8 4.2 2.8 6.7-.1 5.2-4.3 9.4-9.5 9.4zm5.1-7.1c-.3-.1-1.7-.9-1.9-1-.3-.1-.5-.1-.7.1-.2.3-.8 1-.9 1.1-.2.2-.3.2-.6.1s-1.2-.5-2.3-1.4c-.9-.8-1.4-1.7-1.6-2-.2-.3 0-.5.1-.6s.3-.3.4-.5c.2-.1.3-.3.4-.5.1-.2 0-.4 0-.5C10 9 9.3 7.6 9 7c-.1-.4-.4-.3-.5-.3h-.6s-.4.1-.7.3c-.3.3-1 1-1 2.4s1 2.8 1.1 3c.1.2 2 3.1 4.9 4.3.7.3 1.2.5 1.6.6.7.2 1.3.2 1.8.1.6-.1 1.7-.7 1.9-1.3.2-.7.2-1.2.2-1.3-.1-.3-.3-.4-.6-.5z"/></svg>
                            </div>
                          </div>
                        </a>
                  </ShareArea>
                </ShareContentArea>
          </NewsContainer>

        </Drawer>
      </>
    );
  }
);
