import React, { useEffect, useState } from "react";
import { Select } from "antd";
import { useSelector } from "react-redux";

const { Option } = Select;

export default ({ onSelect, initialValue = null, disabled = false }) => {
  const [selected, setSelected] = useState(initialValue);
  const { categories } = useSelector((store) => store.discountClub);

  useEffect(() => {
    if (initialValue) {
      setSelected(initialValue);
      onSelect(initialValue);
    }
  }, [initialValue]);

  const onSelectCategory = (categoryId) => {
    setSelected(categoryId);
    onSelect(categoryId);
  };

  return (
    <Select
      value={selected}
      placeholder="Filtrar por Categoria"
      style={{ width: 220 }}
      onChange={onSelectCategory}
      disabled={disabled}
      showSearch
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      <Option value={null}>Selecionar Categoria</Option>
      {categories.map((category) => (
        <Option key={category.id} value={category.id}>
          {category.name}
        </Option>
      ))}
    </Select>
  );
};
