import React, { useEffect, useState } from "react";
import { Col, Descriptions, Divider, Input, Row, Select, Table } from "antd";
import { notifyError } from "../../helpers/notifications";
import Can from "../../rbac/Can";
import StateFilter from "../forms/StateFilter";
import { createDiscountsListenerService } from "../../services/discountClub";
import DiscountClubCategoriesFilter from "./DiscountClubCategoriesFilter";
import DiscountOptions from "./DiscountOptions";

export default () => {
  const [discounts, setDiscounts] = useState(null);
  const [loading, isLoading] = useState(false);
  const [selectedState, setSelectedState] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [statusFilter, setStatusFilter] = useState(null);
  const [search, setSearch] = useState(null);

  useEffect(() => {
    getDiscounts();
  }, [selectedState, selectedCategory, statusFilter]);

  const getDiscounts = async () => {
    try {
      isLoading(true);

      createDiscountsListenerService(
        {
          filters: {
            stateId: selectedState,
            categoryId: selectedCategory,
            disabled: statusFilter,
          },
        },
        (discounts) => {
          setDiscounts(discounts);
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Desconto",
      dataIndex: "discount",
      key: "discount",
      width: "15%",
    },
    {
      title: "Descrição",
      key: "discount_description",
      dataIndex: "discount_description",
      width: "20%",
    },
    {
      title: "Local",
      key: "company",
      dataIndex: "company",
      width: "10%",
    },
    {
      title: "Categoria",
      key: "categoryDetails.name",
      dataIndex: "categoryDetails.name",
      width: "15%",
    },
    {
      title: "Endereço",
      key: "address",
      width: "30%",
      render: (data) => (
        <>
          <p>{data.place}</p>
          <small>{data.address}</small>
        </>
      ),
    },
    {
      title: "Estado",
      dataIndex: "stateDetails.uf",
      key: "stateDetails.uf",
    },
    {
      title: "Ações",
      key: "id",
      render: (discount) => <DiscountOptions discount={discount} />,
    },
  ];

  const discountsFilter = () => {
    if (search && search !== "") {
      return discounts.filter(
        (discount) =>
          discount.discount_description
            .toLowerCase()
            .includes(search.toLowerCase()) ||
          discount.company.toLowerCase().includes(search.toLowerCase())
      );
    }
    return discounts;
  };

  return (
    <div>
      <Row gutter={24}>
        <Col span={4}>
          <Input.Search
            maxLength={15}
            onPressEnter={(v) => setSearch(v.target?.value)}
            onSearch={(v) => setSearch(v.target?.value)}
            allowClear
            placeholder="Descrição ou Local"
          />
        </Col>
        <Col span={4}>
          <Can
            perform="news:see_all"
            yes={() => (
              <StateFilter onSelect={(state) => setSelectedState(state)} />
            )}
          />
        </Col>
        <Col span={4}>
          <Can
            perform="news:see_all"
            yes={() => (
              <DiscountClubCategoriesFilter
                onSelect={(category) => setSelectedCategory(category)}
              />
            )}
          />
        </Col>
        <Col span={4}>
          <Select
            value={statusFilter}
            placeholder="Filtrar por status de acesso"
            style={{ width: 220 }}
            onChange={(status) => setStatusFilter(status)}
            showSearch
            filterOption={(input, option) =>
              option.props.children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            <Select.Option value={null}>Selecionar Status</Select.Option>
            <Select.Option value={true}>Inativo</Select.Option>
            <Select.Option value={false}>Ativo</Select.Option>
          </Select>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={discountsFilter()}
            loading={loading}
            rowExpandable
            expandedRowRender={(discount) => (
              <DiscountDetail discount={discount} />
            )}
          />
        </Col>
      </Row>
    </div>
  );
};

const DiscountDetail = ({ discount }) => {
  return (
    <Row gutter={16}>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Detalhes do Desconto
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Desconto">
            {discount.discount}
          </Descriptions.Item>
          <Descriptions.Item label="Descrição">
            {discount.discount_description}
          </Descriptions.Item>
          <Descriptions.Item label="Local">
            {discount.company}
          </Descriptions.Item>
          <Descriptions.Item label="Marca">
            <img src={discount.brand} height={200} alt="" />
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Endereço">
            {discount.address}
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};
