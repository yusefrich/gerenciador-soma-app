import React, { useState } from "react";
import {
  Button,
  Col,
  Drawer,
  Form,
  Icon,
  Input,
  message,
  Row,
  Upload,
} from "antd";

import {
  deleteImageFromUrlService,
  uploadFileService,
} from "../../services/storage";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { createNewDiscountClubCategory } from "../../services/discountClub";

function beforeUpload(file) {
  const isPng = file.type === "image/png";
  const isLt2M = file.size / 1024 / 1024 < 1;

  if (!isPng) {
    message.error(
      "Você só pode realizar o upload de imagem do tipo PNG, de preferência com o fundo transparente"
    );
  } else if (!isLt2M) {
    message.error("Imagem deve ser menor que 1MB!");
  }
  return isPng && isLt2M;
}

export default Form.create({ name: "add_discount_club_category" })(
  ({ form: { getFieldDecorator, validateFieldsAndScroll, resetFields } }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [loading, isLoading] = useState(false);
    const [buttonDisabled, isButtonDisabled] = useState(false);
    const [uploadingImage, isUploadingImage] = useState(false);
    const [imageUrl, setImageUrl] = useState("");

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            values.icon = imageUrl;

            await createNewDiscountClubCategory(values);
            notifySuccess({
              title: "Ok!",
              message: "Uma nova categoria para o club de desconto foi criada ",
            });
            resetFields();
            setImageUrl("");
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const onUpload = ({ onSuccess, onError, file, onProgress }) => {
      const onSuccessCallback = (url) => {
        setImageUrl(url);
        onSuccess("done");
      };
      uploadFileService({
        url: "discounts_club_categories",
        onSuccessCallback,
        onError,
        file,
        onProgress,
      });
    };

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        isUploadingImage(true);
        isButtonDisabled(true);
        setImageUrl("");
        return;
      }
      if (info.file.status === "done") {
        isButtonDisabled(false);
        isUploadingImage(false);
      }
    };

    const onCancelForm = async () => {
      if (imageUrl) deleteImageFromUrlService(imageUrl);
      isDrawerOpen(false);
      setImageUrl("");
    };

    return (
      <>
        <Button type="primary" onClick={() => isDrawerOpen(!drawerOpen)}>
          <Icon type="plus" /> Nova Categoria
        </Button>
        <Drawer
          title="Criar Nova Categoria"
          width={520}
          onClose={onCancelForm}
          visible={drawerOpen}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Categoria">
                  {getFieldDecorator("name", {
                    rules: [
                      {
                        required: true,
                        message: "Insira um nome para a categoria",
                      },
                    ],
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Ícone">
                  {getFieldDecorator("icon", {
                    rules: [{ required: true, message: "Insira um ícone" }],
                  })(
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      beforeUpload={beforeUpload}
                      customRequest={onUpload}
                      onChange={handleChangeImage}
                    >
                      {imageUrl ? (
                        <img
                          src={imageUrl}
                          alt="avatar"
                          style={{ width: "100%" }}
                        />
                      ) : (
                        <div>
                          <Icon type={uploadingImage ? "loading" : "plus"} />
                          <div className="ant-upload-text">Upload</div>
                        </div>
                      )}
                    </Upload>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Button onClick={onCancelForm} style={{ marginRight: 8 }}>
                Cancelar
              </Button>
              <Button
                disabled={buttonDisabled}
                loading={loading}
                htmlType="submit"
                type="primary"
              >
                Salvar
              </Button>
            </div>
          </Form>
        </Drawer>
      </>
    );
  }
);
