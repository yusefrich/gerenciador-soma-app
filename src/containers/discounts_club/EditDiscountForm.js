import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Tag,
  Upload,
} from "antd";
import GoogleMapReact from "google-map-react";

import {
  deleteImageFromUrlService,
  uploadFileService,
} from "../../services/storage";
import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { useSelector } from "react-redux";
import { userCan } from "../../rbac/Can";
import { beforeUpload } from "../../helpers/forms";
import { getAddressService } from "../../services/commonn";
import mapPin from "../../assets/imgs/pin.svg";
import {
  editDiscountService,
  getDiscountClubCategoriesService,
} from "../../services/discountClub";

const { Option } = Select;
const { Search } = Input;

const MapPin = ({ text }) => (
  <div>
    <img src={mapPin} width={20} alt="" />
  </div>
);

export default Form.create({ name: "edit_discount_form" })(
  ({
    discount,
    isOpen,
    onClose,
    form: { getFieldDecorator, validateFieldsAndScroll, resetFields },
  }) => {
    const [loading, isLoading] = useState(false);
    const [loadingAddress, isLoadingAddress] = useState(false);
    const [mapsCenter, setMapsCenter] = useState(discount.mapCoordinates);
    const [buttonDisabled, isButtonDisabled] = useState(false);
    const [uploadingImage, isUploadingImage] = useState(false);
    const [windowWidth, setWindowWidth] = useState("520");

    const [address, setAddress] = useState("");
    const [draggableMap, setDraggableMap] = useState(true);

    const [brandUrl, setBrandUrl] = useState(discount.brand);
    const [categories, setCategories] = useState([]);
    const { states } = useSelector((state) => state.states);
    let timer;

    const user = getUserFromStore();

    useEffect(() => {
      getCategories();
      return () => (timer = null);
    }, []);

    const getCategories = async () => {
      setCategories(await getDiscountClubCategoriesService());
    };

    const handleSearchAddress = async (query) => {
      isLoadingAddress(true);
      const address = await getAddressService(query);
      setMapsCenter(address.geometry.location);
      isLoadingAddress(false);
    };

    const handleSearchAddressInput = (e) => {
      clearTimeout(timer);
      setAddress(e.target.value);
      timer = setTimeout(() => {
        handleSearchAddress(address);
      }, 1000);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            if (brandUrl) values.brand = brandUrl;

            await editDiscountService(discount.id, values);
            notifySuccess({
              title: "Ok!",
              message: "O desconto foi atualizado",
            });
            resetFields();
            setBrandUrl("");
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const onUpload = ({ onSuccess, onError, file, onProgress }) => {
      const onSuccessCallback = (url) => {
        setBrandUrl(url);
        onSuccess("done");
      };
      uploadFileService({
        url: `discounts/${user.id}/brands`,
        onSuccessCallback,
        onError,
        file,
        onProgress,
      });
    };

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        isUploadingImage(true);
        isButtonDisabled(true);
        setBrandUrl("");
        return;
      }
      if (info.file.status === "done") {
        isButtonDisabled(false);
        isUploadingImage(false);
      }
    };

    const IncreaseWindowSizeButton = () => {
      return (
        <Icon
          type={windowWidth === 520 ? "fullscreen" : "fullscreen-exit"}
          onClick={() => setWindowWidth(windowWidth === 520 ? "90%" : 520)}
          style={{ marginRight: 20 }}
        />
      );
    };

    const onCancelForm = async () => {
      onClose();
    };

    const onMoveMap = (childKey, childProps, mouse) => {
      setMapsCenter({ lat: mouse.lat, lng: mouse.lng });
      setDraggableMap(false);
    };

    return (
      <>
        {isOpen && (
          <Drawer
            title={[<IncreaseWindowSizeButton />, "Adicionar Desconto"]}
            width={windowWidth}
            onClose={onCancelForm}
            visible={isOpen}
            bodyStyle={{ paddingBottom: 80 }}
          >
            <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
              <Row gutter={16}>
                <Col span={24}>
                  <Form.Item label="Desconto">
                    {getFieldDecorator("discount", {
                      initialValue: discount.discount,
                      rules: [{ required: true, message: "Insira o desconto" }],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Descrição">
                    {getFieldDecorator("discount_description", {
                      initialValue: discount.discount_description,
                      rules: [
                        { required: true, message: "Insira uma Descrição" },
                      ],
                    })(<Input.TextArea rows={5} />)}
                  </Form.Item>
                  <Form.Item label="Nome da Empresa">
                    {getFieldDecorator("company", {
                      initialValue: discount.company,

                      rules: [
                        {
                          required: true,
                          message: "Insira o nome da empresa",
                        },
                      ],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Imagem da empresa">
                    {getFieldDecorator("brand", {
                      initialValue: discount.brand,
                      rules: [
                        {
                          required: true,
                          message: "Insira a marca da empresa",
                        },
                      ],
                    })(
                      <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        beforeUpload={beforeUpload}
                        customRequest={onUpload}
                        onChange={handleChangeImage}
                      >
                        {brandUrl ? (
                          <img
                            src={brandUrl}
                            alt="avatar"
                            style={{ width: "100%" }}
                          />
                        ) : (
                          <div>
                            <Icon type={uploadingImage ? "loading" : "plus"} />
                            <div className="ant-upload-text">Upload</div>
                          </div>
                        )}
                      </Upload>
                    )}
                  </Form.Item>
                  <Divider />
                  <Form.Item label="Categorias">
                    {getFieldDecorator("category", {
                      initialValue: discount.categoryId,
                      rules: [
                        { required: true, message: "Selecione uma categoria" },
                      ],
                    })(
                      <Select>
                        {categories.map((category) => (
                          <Option key={category.id} value={category.id}>
                            {category.name}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                  <Form.Item label="Subcategoria">
                    {getFieldDecorator("subcategory", {
                      initialValue: discount.subcategory,

                      rules: [
                        {
                          required: true,
                          message: "Insira a subcategoria",
                        },
                      ],
                    })(<Input placeholder="Ex.: Comida Brasileira" />)}
                  </Form.Item>
                  <Divider />

                  <Form.Item label="Estado">
                    {getFieldDecorator("state", {
                      initialValue: discount.state,
                      rules: [
                        { required: true, message: "Selecione uma estado" },
                      ],
                    })(
                      <Select
                        disabled={!userCan({ perform: "news:edit_state" })}
                      >
                        {states.map((state) => (
                          <Option key={state.id} value={state.id}>
                            {state.name}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                  <Divider />
                  <Form.Item label="Endereço">
                    {getFieldDecorator("address", {
                      initialValue: discount.address,

                      rules: [
                        { required: true, message: "Informe o endereço" },
                      ],
                    })(
                      <Search
                        onChange={handleSearchAddressInput}
                        onPressEnter={handleSearchAddress}
                        placeholder="Pesquise o endereço"
                        enterButton
                        onSearch={handleSearchAddress}
                        loading={loadingAddress}
                      />
                    )}
                  </Form.Item>

                  <Form.Item>
                    <div style={{ width: "100%", height: 300 }}>
                      {getFieldDecorator("mapCoordinates", {
                        initialValue: discount.mapCoordinates,

                        rules: [
                          { required: true, message: "Selecione o Local" },
                        ],
                      })(
                        <GoogleMapReact
                          draggable={draggableMap}
                          bootstrapURLKeys={{
                            key: "AIzaSyChxNdxSPluskBuBgvgkgtrvtdnRu6VQTM",
                          }}
                          center={{
                            lat: mapsCenter.lat,
                            lng: mapsCenter.lng,
                          }}
                          onChildMouseUp={() => setDraggableMap(true)}
                          onChildMouseDown={onMoveMap}
                          onChildMouseMove={onMoveMap}
                          defaultZoom={15}
                        >
                          <MapPin lat={mapsCenter.lat} lng={mapsCenter.lng} />
                        </GoogleMapReact>
                      )}
                    </div>
                  </Form.Item>
                </Col>
              </Row>

              <div
                style={{
                  position: "absolute",
                  right: 0,
                  bottom: 0,
                  width: "100%",
                  borderTop: "1px solid #e9e9e9",
                  padding: "10px 16px",
                  background: "#fff",
                  textAlign: "right",
                }}
              >
                <Button onClick={onCancelForm} style={{ marginRight: 8 }}>
                  Cancelar
                </Button>
                <Button
                  disabled={buttonDisabled}
                  loading={loading}
                  htmlType="submit"
                  type="primary"
                >
                  Salvar
                </Button>
              </div>
            </Form>
          </Drawer>
        )}
      </>
    );
  }
);
