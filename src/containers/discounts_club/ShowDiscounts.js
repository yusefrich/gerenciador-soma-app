import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Tag,
  Upload,
} from "antd";
import GoogleMapReact from "google-map-react";

import {
  deleteImageFromUrlService,
  uploadFileService,
} from "../../services/storage";
import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { useSelector } from "react-redux";
import { userCan } from "../../rbac/Can";
import { beforeUpload } from "../../helpers/forms";
import { getAddressService } from "../../services/commonn";
import mapPin from "../../assets/imgs/pin.svg";
import {
  editDiscountService,
  getDiscountClubCategoriesService,
} from "../../services/discountClub";
import styled from "styled-components";

const { Option } = Select;
const { Search } = Input;

const MapPin = ({ text }) => (
  <div>
    <img src={mapPin} width={20} alt="" />
  </div>
);


const DiscountContainer = styled.div`
display: flex;
justify-content: between;
margin-bottom: 15px;
margin-left: 15px;
margin-right: 15px;
cursor: pointer;
`;
const DiscountTitleContainer = styled.div`
margin-left: 20px
`;
const DiscountTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: 500;
font-size: 18px;
line-height: 240%;
color: #9096B4;
`;
const DiscountSubTitle = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 16px;
line-height: 175.9%;
color: #9096B4;
`;
const EventButtonA = styled.a`
background: #0F4DFF;
border-radius: 57.5875px;
color: #fff !important;
padding: 8px 45px;
border: none;
cursor: pointer;
position: relative;
top: 30px;

font-family: Poppins;
font-style: normal;
font-weight: bold;
font-size: 14.3467px;
line-height: 175.9%;
`;


const DrawerContentDetail = styled.div`
text-align: center;
margin: 15px;
`;
const DetailTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: bold;
font-size: 22px;
line-height: 120%;
margin-bottom: 15px;
`;
const DetailParagraph = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 14px;
line-height: 120%;
margin-bottom: 56px;

`;
const DetailShoTitle = styled.h4`
font-family: Poppins;
font-style: normal;
font-weight: 500;
font-size: 18px;
line-height: 240%;
margin-bottom: 16px;

`;
const DetailShoLocal = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 14px;
line-height: 120%;
`;

export default Form.create({ name: "edit_discount_form" })(
  ({
    discount,
    form: { getFieldDecorator, validateFieldsAndScroll, resetFields },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [loading, isLoading] = useState(false);
    const [loadingAddress, isLoadingAddress] = useState(false);
    const [mapsCenter, setMapsCenter] = useState(discount.mapCoordinates);
    const [buttonDisabled, isButtonDisabled] = useState(false);
    const [uploadingImage, isUploadingImage] = useState(false);
    const [windowWidth, setWindowWidth] = useState("520");

    const [address, setAddress] = useState("");
    const [draggableMap, setDraggableMap] = useState(true);

    const [brandUrl, setBrandUrl] = useState(discount.brand);
    const [categories, setCategories] = useState([]);
    const { states } = useSelector((state) => state.states);
    let timer;

    const user = getUserFromStore();

    useEffect(() => {
      getCategories();
      return () => (timer = null);
    }, []);

    const getCategories = async () => {
      setCategories(await getDiscountClubCategoriesService());
    };

    const handleSearchAddress = async (query) => {
      isLoadingAddress(true);
      const address = await getAddressService(query);
      setMapsCenter(address.geometry.location);
      isLoadingAddress(false);
    };

    const handleSearchAddressInput = (e) => {
      clearTimeout(timer);
      setAddress(e.target.value);
      timer = setTimeout(() => {
        handleSearchAddress(address);
      }, 1000);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            if (brandUrl) values.brand = brandUrl;

            await editDiscountService(discount.id, values);
            notifySuccess({
              title: "Ok!",
              message: "O desconto foi atualizado",
            });
            resetFields();
            setBrandUrl("");
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const onUpload = ({ onSuccess, onError, file, onProgress }) => {
      const onSuccessCallback = (url) => {
        setBrandUrl(url);
        onSuccess("done");
      };
      uploadFileService({
        url: `discounts/${user.id}/brands`,
        onSuccessCallback,
        onError,
        file,
        onProgress,
      });
    };

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        isUploadingImage(true);
        isButtonDisabled(true);
        setBrandUrl("");
        return;
      }
      if (info.file.status === "done") {
        isButtonDisabled(false);
        isUploadingImage(false);
      }
    };

    const IncreaseWindowSizeButton = () => {
      return (
        <Icon
          type={windowWidth === 520 ? "fullscreen" : "fullscreen-exit"}
          onClick={() => setWindowWidth(windowWidth === 520 ? "90%" : 520)}
          style={{ marginRight: 20 }}
        />
      );
    };

    const onCancelForm = async () => {
      if (brandUrl) deleteImageFromUrlService(brandUrl);
      isDrawerOpen(false);
      setBrandUrl("");
    };

    const onMoveMap = (childKey, childProps, mouse) => {
      setMapsCenter({ lat: mouse.lat, lng: mouse.lng });
      setDraggableMap(false);
    };

    return (
      <>
        {/* <Tag
          color="blue"
          type="primary"
          onClick={() => isDrawerOpen(!drawerOpen)}
        >
          <Icon type="edit" /> Editar
        </Tag> */}
        <DiscountContainer onClick={() => isDrawerOpen(!drawerOpen)} >
          <img style={{borderRadius: 90, marginTop: 20}} src={discount.brand} width="60px" height="60px" alt=""/>
          <DiscountTitleContainer>
            <DiscountTitle>{discount.company}</DiscountTitle>
            <DiscountSubTitle>{discount.subcategory}</DiscountSubTitle>
          </DiscountTitleContainer>
        </DiscountContainer>
        <Divider style={{background: "#090C1D" }}/>
        {drawerOpen && (
          <Drawer
            title={[<IncreaseWindowSizeButton />, "Detalhes do desconto"]}
            width={windowWidth}
            onClose={onCancelForm}
            visible={drawerOpen}
            bodyStyle={{ paddingBottom: 80 }}
          >
            <DrawerContentDetail>

              <img style={{borderRadius: 90, marginTop: 20}} src={discount.brand} width="150px" height="150px" alt=""/>
              
              <DetailTitle> {discount.discount}</DetailTitle>
              <DetailParagraph> {discount.discount_description} </DetailParagraph>
              <DetailShoTitle> {discount.company} </DetailShoTitle>
              <DetailShoLocal> {discount.address} </DetailShoLocal>
              <div style={{ width: "100%", height: 300 }}>
                      {getFieldDecorator("mapCoordinates", {
                        initialValue: discount.mapCoordinates,

                        rules: [
                          { required: true, message: "Selecione o Local" },
                        ],
                      })(
                        <GoogleMapReact
                          draggable={draggableMap}
                          bootstrapURLKeys={{
                            key: "AIzaSyChxNdxSPluskBuBgvgkgtrvtdnRu6VQTM",
                          }}
                          center={{
                            lat: mapsCenter.lat,
                            lng: mapsCenter.lng,
                          }}
                          defaultZoom={15}
                        >
                          <MapPin lat={mapsCenter.lat} lng={mapsCenter.lng} />
                        </GoogleMapReact>
                      )}
                    </div>
                    <EventButtonA style={{marginLeft: 20}} target="_blank" href={"https://www.google.com/maps/search/?api=1&query="+mapsCenter.lat+","+mapsCenter.lng}>Como chegar</EventButtonA>

            </DrawerContentDetail>

          </Drawer>
        )}
      </>
    );
  }
);
