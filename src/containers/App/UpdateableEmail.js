import React, { useState } from "react";
import UpdateableItem from "../../components/UpdateableItem";
import { getUserFromStore } from "../../helpers/store";
import Spin from "../../components/Spin";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { updateEmailService } from "../../services/user";

export default ({ user }) => {
  const [loading, isLoading] = useState(false);
  const userToBeChanged = user || getUserFromStore();

  const updateEmail = async ({ email }) => {
    try {
      isLoading(true);
      await updateEmailService(userToBeChanged.id, email);
      notifySuccess({ title: "O email foi alterado com sucesso" });
      isLoading(false);
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  if (loading) return <Spin />;

  return (
    <UpdateableItem
      extraClass="full-width"
      value={userToBeChanged.email || userToBeChanged._email}
      field="email"
      onChange={updateEmail}
    />
  );
};
