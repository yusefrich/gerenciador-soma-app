import React, { useState } from "react";
import { Button, Checkbox, Form, Icon, Input } from "antd";
import { notifyError } from "../../helpers/notifications";
import { signInWithEmailPassword } from "../../services/auth";
import ModalForgetPassword from "./modalForgetPassword";

export default Form.create({ name: "auth_login" })(
  ({ form: { getFieldDecorator, validateFields } }) => {
    const [loading, isLoading] = useState(false);
    const [modalForgetPassword, isOpenmodalForgetPassword] = useState(false);

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFields(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await signInWithEmailPassword(values.email, values.password);
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    return (
      <Form onSubmit={handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator("email", {
            rules: [{ required: true, message: "Insira o Email" }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder="Email"
            />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Insira a senha!" }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
              type="password"
              placeholder="Senha"
            />
          )}
        </Form.Item>
        <Form.Item>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a
            onClick={() => isOpenmodalForgetPassword(true)}
            className="login-form-forgot block text-right"
            href="#"
          >
            Esqueceu a senha?
          </a>
          <br />
          {getFieldDecorator("remember", {
            valuePropName: "checked",
            initialValue: true,
          })(<Checkbox>Lembrar de mim?</Checkbox>)}
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            loading={loading}
            className="login-form-button"
          >
            Entrar
          </Button>
        </Form.Item>
        <ModalForgetPassword
          visible={modalForgetPassword}
          handleCancel={() => isOpenmodalForgetPassword(false)}
        />
      </Form>
    );
  }
);
