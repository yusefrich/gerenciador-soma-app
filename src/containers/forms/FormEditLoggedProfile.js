import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Form, Input, Row, Select, Checkbox } from "antd";

import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { updateUserService } from "../../services/user";
import { StateActions } from "../../ducks/actions";
import { getCitiesService } from "../../services/state";

const { Option } = Select;

export default Form.create({ name: "form_edit_logged_profile" })(
  ({ form: { getFieldDecorator, validateFieldsAndScroll } }) => {
    const dispatch = useDispatch();
    const [loading, isLoading] = useState(false);
    const [cities, setCities] = useState([]);
    const { states } = useSelector((store) => store.states);

    const user = getUserFromStore();

    useEffect(() => {
      getStates();
      getCities(user.state.id);
    }, [user]);

    const getStates = async () => {
      dispatch(StateActions.getStates());
    };

    const getCities = async (stateId) => {
      if (stateId !== user.state.id) user.city = "";
      const cities = await getCitiesService(stateId);
      setCities(cities);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await updateUserService(user.id, values);
            notifySuccess({
              title: "Tudo certo",
              message: "Os dados foram atualizados",
            });
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    return (
      <Form onSubmit={handleSubmit}>
        <Row gutter={24}>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Form.Item label="Nome">
              {getFieldDecorator("name", {
                initialValue: user.name,
                rules: [
                  {
                    required: true,
                    message: "Por favor insira o nome",
                  },
                ],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="CPF">
              {getFieldDecorator("cpf", {
                initialValue: user.cpf,
                rules: [
                  {
                    required: true,
                    message: "Por favor insira o CPF",
                  },
                ],
              })(<Input disabled={Boolean(user.cpf)} />)}
            </Form.Item>

            <Form.Item label="Celular">
              {getFieldDecorator("cellphone", {
                initialValue: user.cellphone,
                rules: [
                  {
                    required: true,
                    message: "Por favor insira um número de telefone",
                  },
                ],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Estado">
              {getFieldDecorator("state", {
                initialValue: user.state && user.state.id,
                rules: [{ required: true, message: "Selecione o estado" }],
              })(
                <Select
                  onChange={getCities}
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {states.map((state) => (
                    <Option
                      key={state.id}
                      value={state.id}
                    >{`${state.name}-${state.uf}`}</Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label="Cidade">
              {getFieldDecorator("city", {
                initialValue: user.city,
              })(
                <Select
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {cities.map((city) => (
                    <Option key={city.id} value={city.id}>
                      {city.name}
                    </Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label="Whatsapp">
              <Row gutter={8}>
                <Col span={12}>
                  {getFieldDecorator("phone", {
                    initialValue: user.phone,
                    rules: [
                      {
                        required: true,
                        message: "Por favor insira o Whatsapp",
                      },
                    ],
                  })(<Input />)}
                </Col>
                <Col span={12}>
                  {getFieldDecorator("public_phone", {
                    initialValue: user.public_phone,
                  })(<Checkbox>Deixar número público</Checkbox>)}
                </Col>
              </Row>
            </Form.Item>

            <Form.Item label="Instagram">
              <Row gutter={8}>
                <Col span={12}>
                  {getFieldDecorator("instagram", {
                    initialValue: user.instagram,
                  })(<Input />)}
                </Col>
                <Col span={12}>
                  {getFieldDecorator("public_instagram", {
                    initialValue: user.public_instagram,
                  })(<Checkbox>Deixar instagram público</Checkbox>)}
                </Col>
              </Row>
            </Form.Item>

            <Form.Item label="Sobre Você">
              {getFieldDecorator("aboutMe", {
                initialValue: "",
              })(<Input.TextArea rows={4} />)}
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Form.Item label="Empresa">
              {getFieldDecorator("business", {
                initialValue: user.business,
                rules: [
                  {
                    required: true,
                    message: "Por favor insira o nome da sua empresa",
                  },
                ],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="CNPJ">
              {getFieldDecorator("cnpj", {
                initialValue: user.cnpj,
                rules: [
                  {
                    required: true,
                    message: "Por favor insira o CNPJ",
                  },
                ],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="Quantidade de funcionários">
              {getFieldDecorator("employees", {
                initialValue: user.employees,
                rules: [
                  {
                    required: true,
                    message:
                      "Por favor insira quantidade de funcionários da sua empresa",
                  },
                ],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="Média de faturamento do negócio">
              {getFieldDecorator("billing_average", {
                initialValue: user.billing_average,
                rules: [
                  {
                    required: true,
                    message:
                      "Por favor insira a quantidade de faturamento média do seu negócio",
                  },
                ],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="O que você considera ponto forte na sua personalidade empreendedora?">
              {getFieldDecorator("strongPoints", {
                initialValue: "",
              })(<Input.TextArea rows={4} />)}
            </Form.Item>

            <Form.Item label="Você possui alguma dúvida quanto empreendedor?">
              {getFieldDecorator("entrepreneurDoubts", {
                initialValue: "",
              })(<Input.TextArea rows={4} />)}
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                loading={loading}
                className="login-form-button"
              >
                Salvar
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    );
  }
);
