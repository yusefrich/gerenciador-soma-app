import React, { useState } from "react";
import Spin from "../../components/Spin";
import DatePicker from "../../components/DatePicker";
import { updateUserService } from "../../services/user";
import { notifySuccess } from "../../helpers/notifications";
import { firestoreTimeToMoment } from "../../helpers/common";

const SetValidityDate = ({ user }) => {
  const [loading, isLoading] = useState(false);

  const setUserValidity = async (date) => {
    try {
      isLoading(true);
      const validity = date ? date.toDate() : null;

      await updateUserService(user.id, { ...user, validity });
      notifySuccess({
        title: "Tudo certo",
        message: "A data de Vigência foi atualizada",
      });
      isLoading(false);
    } catch (e) {
      isLoading(false);
    }
  };

  if (loading) return <Spin />;

  return (
    <DatePicker
      defaultValue={user.validity && firestoreTimeToMoment(user.validity)}
      onChange={setUserValidity}
      placeholder="Data de Vigência"
    />
  );
};

export default SetValidityDate;
