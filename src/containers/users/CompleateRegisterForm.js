import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
} from "antd";

import { notifyError, notifySuccess } from "../../helpers/notifications";
import rolesEnum from "../../models/rolesEnum";
import { createNewUserService, updateUserService } from "../../services/user";
import { getStatesService } from "../../services/state";
import { store } from "../../store";
import { push } from "connected-react-router";
import {useSelector} from 'react-redux';

const { Option } = Select;

export default Form.create({ name: "new_user_form" })(
  ({
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
      validateFields,
      getFieldValue,
    },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [confirmDirty] = useState(false);
    const [states, setStates] = useState([]);
    const [loading, isLoading] = useState(false);
    const {user} = useSelector(state => state.auth);

    useEffect(() => {
      getStates();
    }, []);

    const getStates = async () => {
      const states = await getStatesService();
      setStates(states);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {      
          try {
            isLoading(true);
            console.log("vales being send to create the user");
            console.log(values)
            await updateUserService(user.id, values);
            notifySuccess({
              title: "Tudo certo",
              message: "Um novo acesso foi inserido",
            });
            isLoading(false);
          } catch (e) {
            
            isLoading(false);
            notifyError({
              title: "Erro",
              message: "Erro no registro de usuario, tente novamente",
            });

            notifyError(e);
          }
        }
      });
    };

    const validateToNextPassword = (rule, value, callback) => {
      if (value && confirmDirty) {
        validateFields(["confirm"], { force: true });
      }
      callback();
    };

    const compareToFirstPassword = (rule, value, callback) => {
      if (value && value !== getFieldValue("password")) {
        callback("As senhas não são iguais");
      } else {
        callback();
      }
    };

    return (
      <div className="user_post_form">
        <Form style={{marginTop: 90, marginBottom: 90}} onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col span={24}>
            <Form.Item label="Nome">
                {getFieldDecorator("name", {
                  rules: [{ required: true, message: "Insira um nome" }],
                })(<Input />)}
              </Form.Item>
              <Form.Item label="Cpf">
                {getFieldDecorator("cpf", {
                  rules: [{ required: true, message: "Insira um cpf" }],
                })(<Input type={"number"}/>)}
              </Form.Item>
              <Form.Item label="Celular">
                {getFieldDecorator("cellphone", {
                  rules: [{ required: true, message: "Insira um celular" }],
                })(<Input type={"number"}  />)}
              </Form.Item>
              {/* <Form.Item label="Email">
                {getFieldDecorator("email", {
                  rules: [{ required: true, message: "Insira um email" }],
                })(<Input style={{ width: "100%" }} />)}
              </Form.Item>
              <Form.Item label="Senha" hasFeedback>
                {getFieldDecorator("password", {
                  rules: [
                    {
                      required: true,
                      message: "Insira uma senha!",
                    },
                    {
                      validator: validateToNextPassword,
                    },
                  ],
                })(<Input.Password />)}
              </Form.Item> */}
              <Form.Item label="Confirme a Senha" hasFeedback>
                {getFieldDecorator("confirm", {
                  rules: [
                    {
                      required: true,
                      message: "Repita a senha!",
                    },
                    {
                      validator: compareToFirstPassword,
                    },
                  ],
                })(<Input.Password />)}
              </Form.Item>

              <Form.Item  className="user_select" label="Estado">
                {getFieldDecorator("state", {
                  rules: [{ required: true, message: "Selecione o estado" }],
                })(
                    <Select
                      showSearch
                      filterOption={(input, option) =>
                        option.props.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      {states.map((state) => (
                        <Option
                          key={state.id}
                          value={state.id}
                        >{`${state.name}-${state.uf}`}</Option>
                      ))}
                    </Select>
                )}
              </Form.Item>

              <Form.Item label="Empresa">
                {getFieldDecorator("business", {
                  rules: [{ required: true, message: "Insira uma empresa" }],
                })(<Input style={{ width: "100%" }} />)}
              </Form.Item>
              <Form.Item label="CNPJ">
                {getFieldDecorator("cnpj", {
                  rules: [{ required: true, message: "Insira um cnpj" }],
                })(<Input type={"number"} style={{ width: "100%" }} />)}
              </Form.Item>
              <Form.Item label="Telefone">
                {getFieldDecorator("phone", {
                  rules: [{ required: true, message: "Insira um telefone" }],
                })(<Input type={"number"} style={{ width: "100%" }} />)}
              </Form.Item>
              <Form.Item label="Quantidade de funcinários">
                {getFieldDecorator("employees", {
                  rules: [{ required: true,  message: "Insira a quantidade de funcionarios" }],
                })(<Input type={"number"} style={{ width: "100%" }} />)}
              </Form.Item>
              <Form.Item label="Média de faturamento dos seus negócios">
                {getFieldDecorator("billing_average", {
                  rules: [{ required: true,  message: "Insira o faturamento médio" }],
                })(<Input type={"number"} style={{ width: "100%" }} />)}
              </Form.Item>
              <Form.Item label="O que você considera ponto forte na sua personalidade empreendedora?">
                {getFieldDecorator("strongPoints", {
                  rules: [{ required: false }],
                })(<Input.TextArea />)}
              </Form.Item>
              <Form.Item label="Você possui alguma dúvida quanto empreendedor?">
                {getFieldDecorator("entrepreneurDoubts", {
                  rules: [{ required: false }],
                })(<Input.TextArea />)}
              </Form.Item>

              <Form.Item hidden label="Nível de acesso">
                <Select>
                  <Option selected value={rolesEnum.USER}>Estadual</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Button
            onClick={() => {store.dispatch(push("/login"));}}
            style={{ marginRight: 8 }}
            className="login-form-button-outline"
          >
            Login
              </Button>
          <Button loading={loading} htmlType="submit" type="primary">
            Enviar
              </Button>
        </Form>
      </div>
    );
  }
);
