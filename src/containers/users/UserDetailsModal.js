import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Button, Modal } from "antd";
import Spin from "../../components/Spin";
import { getUserData } from "../../services/user";
import UserDetails from "./UserDetails";
import UserOptions from "./UserOptions";

const UserDetailsModal = () => {
  const [modalOpen, isModalOpen] = useState(false);
  const [loading, isLoading] = useState(false);
  const [user, setUser] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    if (id) {
      getUserDetailsData();
    }
  }, [id]);

  const getUserDetailsData = async () => {
    try {
      isModalOpen(true);
      isLoading(true);
      const user = await getUserData(id);
      setUser(user);
      isLoading(false);
    } catch (e) {
      isModalOpen(false);
    }
  };

  if (!id) return null;

  return (
    <Modal
      title=""
      width="80%"
      onCancel={() => isModalOpen(false)}
      visible={modalOpen}
      footer={[
        user && (
          <Button type="primary" key="options">
            <UserOptions user={user} />
          </Button>
        ),
        <Button key="back" onClick={() => isModalOpen(false)}>
          Fechar
        </Button>,
      ]}
    >
      {loading ? (
        <div style={{ textAlign: "center" }}>
          <Spin />
        </div>
      ) : (
        <>
          <UserDetails user={user} />
        </>
      )}
    </Modal>
  );
};

export default UserDetailsModal;
