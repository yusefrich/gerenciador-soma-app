import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
} from "antd";

import { notifyError, notifySuccess } from "../../helpers/notifications";
import rolesEnum from "../../models/rolesEnum";
import { createNewUserService } from "../../services/user";
import { getStatesService } from "../../services/state";

const { Option } = Select;

export default Form.create({ name: "new_user_form" })(
  ({
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
      validateFields,
      getFieldValue,
    },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [confirmDirty] = useState(false);
    const [states, setStates] = useState([]);
    const [loading, isLoading] = useState(false);

    useEffect(() => {
      getStates();
    }, []);

    const getStates = async () => {
      const states = await getStatesService();
      setStates(states);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await createNewUserService(values);
            notifySuccess({
              title: "Tudo certo",
              message: "Um novo acesso foi inserido",
            });
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const validateToNextPassword = (rule, value, callback) => {
      if (value && confirmDirty) {
        validateFields(["confirm"], { force: true });
      }
      callback();
    };

    const compareToFirstPassword = (rule, value, callback) => {
      if (value && value !== getFieldValue("password")) {
        callback("As senhas não são iguais");
      } else {
        callback();
      }
    };

    return (
      <>
        <Button type="primary" onClick={() => isDrawerOpen(!drawerOpen)}>
          <Icon type="plus" /> Novo usuário
        </Button>
        <Drawer
          title="Criar nova conta"
          width={520}
          onClose={() => isDrawerOpen(false)}
          visible={drawerOpen}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Nome">
                  {getFieldDecorator("name", {
                    rules: [{ required: true, message: "Insira um nome" }],
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Email">
                  {getFieldDecorator("email", {
                    rules: [{ required: true, message: "Insira um email" }],
                  })(<Input style={{ width: "100%" }} />)}
                </Form.Item>
                <Form.Item label="Senha" hasFeedback>
                  {getFieldDecorator("password", {
                    rules: [
                      {
                        required: true,
                        message: "Insira uma senha!",
                      },
                      {
                        validator: validateToNextPassword,
                      },
                    ],
                  })(<Input.Password />)}
                </Form.Item>
                <Form.Item label="Confirme a Senha" hasFeedback>
                  {getFieldDecorator("confirm", {
                    rules: [
                      {
                        required: true,
                        message: "Repita a senha!",
                      },
                      {
                        validator: compareToFirstPassword,
                      },
                    ],
                  })(<Input.Password />)}
                </Form.Item>
                <Divider />
                <Form.Item label="Estado">
                  {getFieldDecorator("state", {
                    rules: [{ required: true, message: "Selecione o estado" }],
                  })(
                    <Select
                      showSearch
                      filterOption={(input, option) =>
                        option.props.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      {states.map((state) => (
                        <Option
                          key={state.id}
                          value={state.id}
                        >{`${state.name}-${state.uf}`}</Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
                <Divider />
                <Form.Item label="Nível de acesso">
                  {getFieldDecorator("role", {
                    rules: [
                      {
                        required: true,
                        message: "Selecione o nível de acesso",
                      },
                    ],
                  })(
                    <Select>
                      <Option value={rolesEnum.ADMIN}>Estadual</Option>
                      <Option value={rolesEnum.SUPER_ADMIN}>Super Admin</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Button
                onClick={() => isDrawerOpen(false)}
                style={{ marginRight: 8 }}
              >
                Cancelar
              </Button>
              <Button loading={loading} htmlType="submit" type="primary">
                Salvar
              </Button>
            </div>
          </Form>
        </Drawer>
      </>
    );
  }
);
