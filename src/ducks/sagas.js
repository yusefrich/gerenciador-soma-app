import { all } from "redux-saga/effects";
import statesSaga from "./states/saga";
import forumSaga from "./forum/saga";
import discountClubSaga from "./discountClub/saga";

export default function* rootSaga(getState) {
  yield all([statesSaga(), forumSaga(), discountClubSaga()]);
}
