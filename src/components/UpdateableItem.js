import { RIEInput, RIESelect, RIETextArea, RIEToggle } from "riek";
import React from "react";
import PropTypes from "prop-types";
import Spin from "./Spin";

export default class UpdateableItem extends React.Component {
  static propTypes = {
    field: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    extraClass: PropTypes.string,
    placeholder: PropTypes.string,
    selectIdKey: PropTypes.string,
    selectTextKey: PropTypes.string,
    options: PropTypes.array,
    loading: PropTypes.bool,
    validate: PropTypes.func,
    textFalse: PropTypes.string,
    textTrue: PropTypes.string,
    type: PropTypes.oneOf(["input", "select", "textarea", "toggle"]),
  };

  static defaultProps = {
    type: "input",
  };

  render() {
    const {
      textTrue,
      textFalse,
      field,
      extraClass,
      validate,
      type,
      options,
      value,
      onChange,
      placeholder,
      loading,
      selectIdKey,
      selectTextKey,
    } = this.props;

    if (loading) return <Spin />;

    switch (type) {
      case "input":
        return (
          <RIEInput
            value={value}
            placeholder={placeholder}
            change={onChange}
            className={`editableComponent ${extraClass} `}
            classEditing="editableComponenteEditing"
            propName={field}
            validate={validate}
          />
        );
      case "select":
        const optionIndex = options.findIndex(
          (option) => option[selectIdKey] === value
        );
        const selected = options[optionIndex];

        return (
          <RIESelect
            value={
              selected
                ? {
                    id: selected[selectIdKey],
                    text: selected[selectTextKey],
                  }
                : { id: null, text: "-" }
            }
            change={(value) => onChange({ [field]: value[field].id })}
            className={`editableComponent ${extraClass} `}
            classEditing="editableComponenteEditing"
            propName={field}
            options={options.map((option) => ({
              id: option[selectIdKey],
              text: option[selectTextKey],
            }))}
          />
        );
      case "textarea":
        return (
          <RIETextArea
            value={value}
            placeholder={placeholder}
            change={onChange}
            className={`editableComponent fw ${extraClass} `}
            classEditing="editableComponenteEditing"
            propName={field}
            validate={validate}
            cols={10}
            rows={4}
          />
        );
      case "toggle":
        return (
          <RIEToggle
            textTrue={textTrue}
            textFalse={textFalse}
            value={value}
            change={onChange}
            className={`editableComponent ant-tag pointer ${extraClass} `}
            placeholder={placeholder}
            propName={field}
          />
        );
    }
  }
}
