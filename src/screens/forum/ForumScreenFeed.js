import React from "react";
import Can from "../../rbac/Can";
import UserContainer from "../../components/UserContainer";
import AddForumPostForm from "../../containers/forum/AddForumPostForm";
import ForumList from "../../containers/forum/feed/ForumList";

export default () => {
  return (
    <UserContainer
      pageHeaderIcon="snippets"
      headerTitle="Fórum"
    >
      {<ForumList />}
    </UserContainer>
  );
};
