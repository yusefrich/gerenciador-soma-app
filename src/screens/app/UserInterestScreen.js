import React from "react";
import { getUserFromStore } from "../../helpers/store";
import FormEditLoggedUserInterests from "../../containers/forms/FormEditLoggedUserInterests";
import Container from "../../components/Container";
import User from "../../models/userModel";
import UpdateableEmail from "../../containers/App/UpdateableEmail";
import Spin from "../../components/Spin";

export default () => {
  const user = getUserFromStore();

  if (!(user instanceof User)) return <Spin />;

  return (
    <>
      <Container headerTitle="Meu Perfil">
        <FormEditLoggedUserInterests/>
      </Container>
      {/* <Container>
        <h3>Alterar Email</h3>
        <UpdateableEmail />
      </Container> */}
    </>
  );
};
