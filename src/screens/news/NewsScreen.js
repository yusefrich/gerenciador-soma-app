import React from "react";
import Can from "../../rbac/Can";
import Container from "../../components/Container";
import AddNewsForm from "../../containers/news/AddNewsForm";
import NewsList from "../../containers/news/NewsList";

export default () => {
  return (
    <Container
      pageHeaderSubtitle="Notícias ativas"
      pageHeaderIcon="snippets"
      headerTitle="Notícias"
      extras={[
        <Can key="users" perform="news:add" yes={() => <AddNewsForm />} />,
      ]}
    >
      <NewsList />
    </Container>
  );
};
