import React, { useEffect } from "react";
import styled from "styled-components";
import { Card, Col, Row, Typography } from "antd";
import { push } from "connected-react-router";
import UserSignUpForm from "../../containers/auth/userSignUpForm";
import backgroundImage from "../../assets/imgs/bg.svg";
import somaLogo from "../../assets/imgs/soma_logo.svg";
import logo from "../../assets/imgs/logo.png";
import { useSelector } from "react-redux";
import { getAuthState } from "../../services/auth";
import { store } from "../../store";

const { Title } = Typography;
const Container = styled.div`
  background-color: #090C1D;
  background-position: bottom;
  background-size: cover;
  background-repeat: no-repeat;
  min-height: 100vh;
`;

const LogoContainer = styled.div`
  margin-bottom: 30px;
`;

const CustomCard = styled(Card)`
  padding: 20px 50px;
  border-radius: 10px;
`;

export default () => {
  const { isLoggedIn } = useSelector((state) => state.auth);

  useEffect(() => {
    getAuthState();
  }, []);

  if (isLoggedIn) {
    store.dispatch(push("/web/news_feed"));
  }
  return (
    <Container style={styles.cont}>
      <Row  style={styles.row} type="flex" justify="center" align="middle">
        <Col span={12} style={styles.formContainer}>
          {/* <LogoContainer>
            <img src={somaLogo} alt="Marca da Qualitare" />
          </LogoContainer> */}
        <UserSignUpForm />
        </Col>
      </Row>
    </Container>
  );
};
const styles = {
  cont: {
    background: '#090C1D',
  },
  row: {
    height: "auto",
  },
  formContainer: {
    textAlign: "center",
  },
};
