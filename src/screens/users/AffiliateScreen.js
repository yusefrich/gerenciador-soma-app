import React from "react";

import UserContainer from "../../components/UserContainer";
import Can from "../../rbac/Can";
import NewUserForm from "../../containers/users/NewUserForm";
import UsersList from "../../containers/users/feed/UsersList";

export default () => {
  return (
    <UserContainer
      /* pageHeaderSubtitle="Visualize as pessoas que usam o applicativo" */
      pageHeaderIcon="contacts"
      headerTitle="Usuários"
      /* extras={[
        <Can key="users" perform="users:add" yes={() => <NewUserForm />} />,
      ]} */
    >
      <UsersList/>
    </UserContainer>
  );
};
