const firebase = require("firebase");
require("firebase/firestore");

const { firebaseData } = require("./fs-export.js");

const firebaseConfig = {
  apiKey: "AIzaSyChxNdxSPluskBuBgvgkgtrvtdnRu6VQTM",
  authDomain: "appsoma-quali2020.firebaseapp.com",
  databaseURL: "https://appsoma-quali2020.firebaseio.com",
  projectId: "appsoma-quali2020",
  storageBucket: "appsoma-quali2020.appspot.com",
  messagingSenderId: "419431636132",
  appId: "1:419431636132:web:a55ddadd8d5ed55f7f9a31",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firestore = firebaseApp.firestore();

firestore.settings({
  host: "localhost:8080",
  ssl: false,
});

async function importDB() {
  try {
    const setDoc = async (path, data) => {
      return firestore.doc(path).set(data);
    };

    console.log("Iniciando população do banco de dados de desenvolvimento");

    for (const [colName, colData] of Object.entries(firebaseData)) {
      for (const [docId, docData] of Object.entries(colData.docs)) {
        await setDoc(`${colName}/${docId}`, docData);

        if (docData.collections) {
          for (const [docCollectionId, docCollectionData] of Object.entries(
            docData.collections.cities
          )) {
            await setDoc(
              `${colName}/${docId}/cities/${docCollectionId}`,
              docCollectionData
            );
          }
        }

        console.log(`collection "${colName}" populada`);
      }
    }

    console.log("Finalizou importação de dados");
    return 0;
  } catch (e) {
    console.log(e);
  }
}

importDB().then(() => ({}));
