const { getUserService } = require("../services/users");
const { firestore } = require("../services/firebase");

exports.updateDiscountClubCategory = async function (data, context) {
  try {
    const { uid } = context.auth;
    const { category, newData } = data;
    const requestUserData = await getUserService(uid);

    if (requestUserData.role !== "SUPER_ADMIN")
      throw { message: "Você não pode processar esta solicitação" };

    const discounts = await getDiscountsByCategory(category.id);

    var batch = firestore.batch();

    for (const singleDiscount of discounts) {
      const discountRef = firestore.doc(`discounts_club/${singleDiscount.id}`);
      batch.update(discountRef, {
        categoryId: category.id,
        categoryDetails: { ...singleDiscount.categoryDetails, ...newData },
      });
    }

    const discountCategoryRef = firestore.doc(
      `discounts_club_categories/${category.id}`
    );
    batch.update(discountCategoryRef, newData);

    await batch.commit();

    return { code: 200, message: "Dados Atualizados" };
  } catch (e) {
    return { code: e.code || 204, message: e.message };
  }
};

exports.deleteDiscountClubCategory = async function (data, context) {
  try {
    const { uid } = context.auth;
    const { category } = data;
    const requestUserData = await getUserService(uid);

    if (requestUserData.role !== "SUPER_ADMIN")
      throw { message: "Você não pode processar esta solicitação" };

    const discounts = await getDiscountsByCategory(category.id);

    var batch = firestore.batch();

    for (const singleDiscount of discounts) {
      const discountRef = firestore.doc(`discounts_club/${singleDiscount.id}`);
      batch.update(discountRef, {
        category: null,
        categoryDetails: null,
      });
    }

    const discountCategoryRef = firestore.doc(
      `discounts_club_categories/${category.id}`
    );
    batch.delete(discountCategoryRef);

    await batch.commit();

    return { code: 200, message: "Dados Atualizados" };
  } catch (e) {
    return { code: e.code || 204, message: e.message };
  }
};

async function getDiscountsByCategory(categoryId) {
  const discountsRef = firestore
    .collection("discounts_club")
    .where("categoryId", "==", categoryId)
    .orderBy("createdAt");
  const discounts = await discountsRef.get();

  return discounts.docs.map((n) => ({ ...n.data(), id: n.id }));
}
