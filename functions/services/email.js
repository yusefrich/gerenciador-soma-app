const nodemailer = require("nodemailer");
const { functions } = require("./firebase");

async function configureEmailClient() {
  const gmailEmail = functions.config().gmail.email;
  const gmailPassword = functions.config().gmail.password;

  return nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: gmailEmail,
      pass: gmailPassword,
    },
  });
}

exports.sendEmail = async ({ to, subject, text, html, attachments }) => {
  const emailClient = await configureEmailClient();
  const mailOptions = {
    from: `Seja Soma <aplicativosoma@gmail.com>`,
    to,
    subject,
    text,
    html,
    attachments,
  };

  try {
    await emailClient.sendMail(mailOptions);
    console.log(`Um email foi enviado para  ${to}`);
  } catch (e) {
    console.log(`Houve uma falha no envio de email para ${to}`);
    console.log(`Motivo do erro ${e.message}`);
    console.log(`Código do erro ${e.code}`);
  }
};
