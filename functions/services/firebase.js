const functions = require("firebase-functions");
const firebase = require("firebase");
const admin = require("firebase-admin");

firebase.initializeApp(functions.config().firebase);
admin.initializeApp(functions.config().firebase);

const firestore = admin.firestore();
exports.auth = admin.auth();
exports.functions = functions;
exports.messaging = admin.messaging();
exports.storageBucket = admin.storage().bucket();

if (process.env.NODE_ENV === "development") {
  console.log("Iniciando functions no modo de desenvolvimento");
}

exports.firestore = firestore;
