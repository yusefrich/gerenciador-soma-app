const { functions } = require("../services/firebase");
const { sendEmail } = require("../services/email");

const sendEmailOnUserCreation = functions.firestore
  .document("users/{userId}")
  .onCreate(async (snap) => {
    const userData = { id: snap.id, ...snap.data() };
    try {
      const emailData = {
        to: "adm@sejasoma.com.br",
        subject: "Novo cadastro realizado no App Seja Soma",
        html: `<h1>Novo Cadastro Realizado</h1>
                <p>Houve um novo cadastro realizado no app Seja Soma. </p>
                <p>
                  <strong>Para validar 
                    <a href="https://admin.sejasoma.com.br/dashboard/users/${userData.id}" target="_blank">clique aqui</a>
                  </strong>
                </p>`,
      };
      await sendEmail(emailData);
      console.log(
        "Notificação enviada após a criação de nova usuária no sistema"
      );
    } catch (e) {
      console.log(
        `Houve uma falha ao ao enviar email de notificação após a criação da usuária: ${userData.name}: ${userData.email} `
      );
      console.log(`Motivo do erro ${e.message}`);
      console.log(`Código do erro ${e.code}`);
    }
  });

exports.sendEmailOnUserCreation = sendEmailOnUserCreation;
