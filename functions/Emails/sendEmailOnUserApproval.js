const { getDoc } = require("../services/firestore");
const { functions } = require("../services/firebase");
const { sendEmail } = require("../services/email");

const sendEmailOnUserApproval = functions.https.onCall(async (data) => {
  const { userId } = data;
  const userData = await getDoc(`users/${userId}`);

  try {
    const emailData = {
      to: userData.email,
      attachments: [
        {
          filename: "negative_brand.png",
          path: __dirname + "/../assets/negative_brand.png",
          cid: "negative_brand",
        },
      ],
      subject: "Boas Vindas, seu cadastro foi Aprovado - Seja Soma",
      html: `<h1>Olá, ${userData.name}.</h1>
                <h3>Parabéns! O seu cadastro foi aprovado.</h3>
                <p>Nessa plataforma compartilhamos conteúdos importantes diariamente. Agora você faz parte do SOMA e terá acesso aos nomes dos demais filiados, bem como à lista de eventos e aos fóruns de discussões criados para incentivar a interação entre os membros.</p>
                <div>
                    <p>Para ter acesso a nossa plataforma facilitadora de negócios basta acessar o link abaixo e ter acesso a todas as informações.</p>
                    <a href="https://sejasoma.page.link/primeiros_passos" target="_blank">Acesse já</a>
                </div>
                
                <div style="margin-top: 50px">
                    <p>Seja bem-vindo e bons negócios!</p>
                    <img src="cid:negative_brand" alt="">
                </div>
                <div>
                    <p>Essa plataforma foi orgulhosamente desenvolvida pela <a href="https://www.qualitare.com/" target="_blank">Qualitare</a></p>
                </div>
`,
    };
    await sendEmail(emailData);
    console.log(
      "Notificação enviada após a aprovação de nova usuária no sistema"
    );

    return {
      code: 200,
      message: "Email de confirmação foi enviado para o usuário",
    };
  } catch (e) {
    console.log(
      `Houve uma falha ao ao enviar email de notificação após a aprovação da usuária: ${userData.name}: ${userData.email} `
    );

    console.log(`Motivo do erro ${e.message}`);
    console.log(`Código do erro ${e.code}`);
    return {
      code: e.code,
      message:
        "Houve uma falha ao ao enviar email de notificação após a aprovação da usuária.",
    };
  }
});

exports.sendEmailOnUserApproval = sendEmailOnUserApproval;
