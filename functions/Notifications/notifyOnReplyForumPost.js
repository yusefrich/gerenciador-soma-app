const { functions, messaging } = require("../services/firebase");
require("moment/locale/pt");
const { notificationTypes } = require("./notificationTypes");
const { getDoc, getColData } = require("../services/firestore");

exports.notifyOnReplyForumPost = functions.firestore
  .document("forum/{postId}/replies/{replyId}")
  .onCreate(async (snap, context) => {
    try {
      const replyData = snap.data();
      const { postId } = context.params;
      const postData = await getDoc(`forum/${postId}`);
      const postAuthorData = await getDoc(`users/${postData.author.id}`);

      let notification = {
        notification: {
          title: `${replyData.author.name} respondeu sua postagem no Fórum`,
          body: replyData.text,
        },
        android: {
          notification: {
            sound: "default",
            tag: "forumPostReply",
          },
        },
        apns: {
          payload: {
            aps: {
              sound: "default",
            },
          },
        },
        data: {
          type: notificationTypes.FORUM_POST_REPLY,
          postId: context.params.postId,
        },
      };

      const sendNotificationTo = async (user, notification) => {
        const devicesToken = user.notificationTokens.map(({ token }) => token);

        const notificationId = await messaging.sendToDevice(
          devicesToken,
          notification,
          {
            // Required for background/quit data-only messages on iOS
            contentAvailable: true,
            // Required for background/quit data-only messages on Android
            priority: "high",
          }
        );
        console.log(
          `Notificação de resposta no fórum foi enviada com sucesso com o notificationId: ${notificationId}. Tokens dos dispositivos: ${devicesToken}`
        );
      };

      const postReplies = await getColData(`forum/${postId}/replies`, {});

      for (const reply of postReplies) {
        const userData = await getDoc(`users/${reply.author.id}`);
        if (
          userData.notificationTokens &&
          userData.notificationTokens.length > 0
        ) {
          notification.notification.title = `${replyData.author.name} respondeu a uma postagem no Fórum`;
          await sendNotificationTo(userData, notification);
        }
      }

      if (
        postAuthorData.notificationTokens &&
        postAuthorData.notificationTokens.length > 0 &&
        postData.receiveNotification === true
      ) {
        await sendNotificationTo(postAuthorData, notification);
      }
    } catch (e) {
      console.log("Error sending notification ao criar evento:", e);
    }
  });
