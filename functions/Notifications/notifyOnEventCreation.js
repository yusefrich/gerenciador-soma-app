const { functions, messaging } = require("../services/firebase");
require("moment/locale/pt");
const { notificationTypes } = require("./notificationTypes");

exports.notifyOnEventCreation = functions.firestore
  .document("events/{eventId}")
  .onCreate(async (snap, context) => {
    try {
      const data = snap.data();
      const notification = {
        notification: {
          title: data.title,
          body: data.description,
          image: data.image,
        },
        android: {
          notification: {
            sound: "default",
            tag: "eventNotification",
          },
        },
        apns: {
          payload: {
            aps: {
              sound: "default",
            },
          },
          fcm_options: {
            image: data.image,
          },
        },
        data: {
          type: notificationTypes.EVENTS,
          eventId: context.params.eventId,
        },
        topic: `eventsNotifications${data.stateDetails.id}`,
      };

      const notificationId = await messaging.send(notification);
      console.log(
        "Notificação para o tópico",
        `eventsNotifications${data.stateDetails.id}`,
        "foi enviada com sucesso com o notificationId: ",
        notificationId
      );
    } catch (e) {
      console.log("Error sending notification ao criar evento:", e);
    }
  });
