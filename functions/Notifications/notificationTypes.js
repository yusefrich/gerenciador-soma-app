exports.notificationTypes = {
  EVENTS: "EventNotification",
  NEW_FORUM_POST: "NewForumPostNotification",
  FORUM_POST_REPLY: "ForumPostReplyNotification",
};

exports.notificationConfig = {
  android: {
    notification: {
      sound: "default",
    },
  },
  apns: {
    payload: {
      aps: {
        sound: "default",
      },
    },
  },
};
