const { createAuthUserService } = require("../services/auth");
const { getUserService, updateUserService } = require("../services/users");
const { firestore, auth, functions } = require("../services/firebase");

const { setDoc, getColData } = require("../services/firestore");

const USER_PATH = (userId) => `users/${userId}`;

exports.createUser = async function (data, context) {
  try {
    const { uid } = context.auth;
    const { name, email, role, state, password } = data;
    const requestUserData = await getUserService(uid);

    if (requestUserData.role !== "SUPER_ADMIN")
      throw { message: "Você não pode processar esta solicitação" };

    if (!name || !email || !role || !state || !password)
      throw { message: "Todas as informações devem ser inseridas" };

    const userData = await createAuthUserService({
      displayName: name,
      email,
      password,
      disabled: false,
    });

    await setDoc(USER_PATH(userData.uid), {
      name,
      email,
      role,
      state,
      isSuspended: false,
    });
    return { code: 200, message: "Dados Atualizados" };
  } catch (e) {
    return { code: e.code || 204, message: e.message };
  }
};

exports.deleteUserDataWhenDeleteUser = functions.firestore
  .document("users/{userId}")
  .onDelete(async (snap, context) => {
    const { userId } = context.params;
    const batch = firestore.batch();

    const deleteDocs = (docs) => {
      docs.forEach((doc) => {
        batch.delete(doc.ref);
      });
    };

    //  deleta noticias que a pessoa criou
    const createdNews = await firestore
      .collection("news")
      .where("author.id", "==", userId)
      .get();
    deleteDocs(createdNews.docs);

    //exclui as eventos que a pessoa criou
    const createdEvents = await firestore
      .collection("events")
      .where("author.id", "==", userId)
      .get();
    deleteDocs(createdEvents.docs);

    // delete posts no fórum que a pessoa criou
    const createdForumPosts = await firestore
      .collection("forum")
      .where("author.id", "==", userId)
      .get();
    deleteDocs(createdForumPosts.docs);

    return await batch.commit();
  });

exports.deleteUser = functions.https.onCall(async (data, context) => {
  try {
    const { userId } = data;
    await auth.deleteUser(userId);
    return { code: 200, message: "Usuário excluído" };
  } catch (e) {
    return { code: e.code, message: "Houve um problema ao excluir o usuário" };
  }
});

exports.updateUserData = functions.https.onCall(async (data, context) => {
  try {
    const { uid } = context.auth;
    const { userId, newUserData } = data;
    const loggedUser = await getUserService(uid);
    const user = await getUserService(userId);

    if (loggedUser.id !== userId && loggedUser.role !== "SUPER_ADMIN")
      throw { message: "Você não pode processar esta solicitação" };

    await auth.updateUser(userId, newUserData);
    await updateUserService(userId, { ...user, ...newUserData });

    return { code: 200, message: "Informações do usuário atualizada" };
  } catch (e) {
    return { code: e.code, message: "Houve um problema ao excluir o usuário" };
  }
});
